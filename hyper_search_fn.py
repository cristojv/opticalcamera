import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from kerastuner.tuners import RandomSearch, Hyperband
from kerastuner import HyperParameters
from tensorflow.keras import optimizers, losses, metrics, callbacks

from datagenerator.generator import RSCameraGenerator
from hyper_search_tuner import HyperConvAutoencoder

def normalize_with_moments(x, axes=[0, 1], epsilon=1e-8):
    mean, variance = tf.nn.moments(x, axes=axes)
    x_normed = (x - mean) / tf.sqrt(variance + epsilon)
    return x_normed

def build_model(hyperparams):
    model = HyperConvAutoencoder(hyperparams)
    model.compile(\
        optimizer=optimizers.Adam(hyperparams.Choice('learning_rate',
                                                     values=[1e-2,1e-3])),
        loss=losses.MeanSquaredError(name="loss_mean_squared_error"),
        metrics=[metrics.MeanSquaredError(name="mean_squared_error")])
    return model

def reload_best_models(num_models, directory, project_name, build_model):
    tuner = Hyperband(build_model,
                      hyperparameters=hp,
                      objective="val_loss",
                      max_epochs=100,
                      factor=3,
                      hyperband_iterations=5,
                      directory=directory,
                      project_name=project_name)
    tuner.reload()
    return tuner.get_best_models(num_models = num_models)

def search_hyperparams(input_filename,
                 tuner_folder,
                 project_name,
                 num_layers=None,
                 dataset_size=1000):
    hp = HyperParameters()
    if num_layers:
        # Fix number of layers
        hp.Fixed('num_layers', value=num_layers)
        print("The number of layers has been fixed to: {}".format(num_layers))
    
    tuner = Hyperband(
        build_model,
        hyperparameters=hp,
        objective="val_loss",
        max_epochs=100,
        factor=3,
        hyperband_iterations=5,
        directory=tuner_folder,
        project_name=project_name,
    )
    tuner.search_space_summary()
    
    with np.load(input_filename) as data:
        x_data = data['x_dat'].astype('float32')
        y_data = data['y_dat'].astype('float32')

        train_factor = 0.9
        val_factor = 0.1
        
        training_generator = RSCameraGenerator(\
            x_data[:int(dataset_size * train_factor)],
            y_data[:int(dataset_size * train_factor)],
            dataset_size * train_factor,
            batch_size=32,
            shuffle=True,
            noise_range=[0.02, 0.3])
        val_generator = RSCameraGenerator(\
            x_data[-int(dataset_size * val_factor):],
            y_data[-int(dataset_size * val_factor):],
            dataset_size * val_factor,
            batch_size=32,
            shuffle=True,
            noise_range=[0.02, 0.3])

        cb_list = [callbacks.EarlyStopping(monitor='val_loss',
                                           mode='min',
                                           verbose=1,
                                           patience=5)]
        
        tuner.search(training_generator,
                     validation_data=val_generator,
                     epochs=100,
                     callbacks=cb_list)
        
        tuner.results_summary()