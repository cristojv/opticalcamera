# %%
import numpy as np
import json
import os
import os.path
import argparse
import cv2

from dsp.snr import snr_bw, snr_equalization

if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument('-i', '--inputfolder', required=True, type=str,
                    help="Input folder")
    ap.add_argument('-cf', '--camerafolder', required=True, type=str,
                    help="Camera folder")
    ap.add_argument('-n', '--projectname', required=True, type=str,
                    help="Project name")
    ap.add_argument('-ow', '--overwrite', required=False, action="store_true",
                    default=False, help="Overwrite previous ber.json")

    args = vars(ap.parse_args())
    input_folder = args["inputfolder"]
    camera_folder = args["camerafolder"]
    project_name = args["projectname"]
    overwrite = args["overwrite"]
    
    results_filename = '{}/{}/ber.json'.format(input_folder,project_name)
    print(results_filename)
    if os.path.exists(results_filename) and not overwrite:
        with open(results_filename) as json_file:
            results = json.load(json_file)
    else:
        results = []
    
    for filename in os.listdir('{}/{}'.format(input_folder, project_name)):
        print(filename)
        if filename.startswith('eval'):

            print('{}/{}/{}'.format(input_folder, project_name, filename))
            values = np.genfromtxt('{}/{}/{}'.format(input_folder, project_name, filename),delimiter=',')

            train_exp=filename[23:27]
            train_loss = values[0]
            train_val_loss = values[1]
            
            print(train_exp)
            if not any(results_i['train_exp']==train_exp for results_i in results):
                print("Creating new entry")
                results_i={'train_exp':train_exp,
                        'train_loss':train_loss,
                        'train_val_loss':train_val_loss,
                        'eval_exposures': []}
                results.append(results_i)
            else:
                print("Not creating new entry")
                results_i = next((sub for sub in results if sub['train_exp']==train_exp))
                results_i['train_loss'] = train_loss
                results_i['train_val_loss'] = train_val_loss
                print(results_i)

        elif filename.startswith('sum'):
            pass
            values = np.genfromtxt('{}/{}/{}'.format(input_folder, project_name, filename),delimiter=',')
            train_exp = filename[22:26]
            eval_exp = filename[47:51]
            volt = filename[-8:-5]
            ber = values[0]
            berm = values[1]
            packets_expected = values[2]
            packets_missed = values[3]

            # NOT NEEDED for the second type of snr stimation
            ss = int(eval_exp)
            if ss == 425:
                ss = 444
            elif ss == 501:
                ss = 520
            elif ss == 293:
                ss = 312

            img1 = cv2.imread('{0}/{1}us/{2}/_01571.jpg'.format(camera_folder,
                                                                eval_exp,
                                                                (int(volt)-180)))
            img2 = cv2.imread('{0}/{1}us/{2}/_01572.jpg'.format(camera_folder,
                                                                eval_exp,
                                                                (int(volt)-180)))
            img3 = cv2.imread('{0}/{1}us/{2}/_01573.jpg'.format(camera_folder,
                                                                eval_exp,
                                                                (int(volt)-180)))
            img1 = (img1[:, :, 1]/255.0)

            psignal1, pnoise1 = snr_equalization(img1, None, None)
            psignal2, pnoise2 = snr_equalization(img2, None, None)
            psignal3, pnoise3 = snr_equalization(img3, None, None)

            snr1 = psignal1/pnoise1
            snr2 = psignal2/pnoise2
            snr3 = psignal3/pnoise3
            snr1 = snr1 if snr1 > 0 else 1e-1
            snr2 = snr2 if snr2 > 0 else 1e-1
            snr3 = snr3 if snr3 > 0 else 1e-1
            snr1 = 10 * np.log10(snr1)
            snr2 = 10 * np.log10(snr2)
            snr3 = 10 * np.log10(snr3)
            snr = np.mean((snr1,snr2,snr3))

            if not any(results_i['train_exp']==train_exp for results_i in results):
                print("Creating new entry")
                # If there is not an entry for train_exp
                # Create everything
                results_i={'train_exp':train_exp,
                           'train_loss':'',
                           'train_val_loss':'',
                           'eval_exposures': [{'exposure': eval_exp,
                                               'voltages': [{'voltage':volt,
                                                             'ber':ber,
                                                             'berm':berm,
                                                             'packets_expected':packets_expected,
                                                             'packets_missed':packets_missed,
                                                             'snr': snr}]}]}
                results.append(results_i)
            else:
                print("Not creating new entry")
                results_i = next((sub for sub in results if sub['train_exp']==train_exp))
                eval_exps = results_i['eval_exposures']

                if not any(eval_exp_i['exposure']==eval_exp for eval_exp_i in eval_exps):
                    print("Creating new entry")
                    # If there is not an entry for eval_exp
                    # Create eval_exp entry
                    eval_exp_i = {'exposure': eval_exp, 'voltages': [{'voltage':volt,
                                                                      'ber':ber,
                                                                      'berm':berm,
                                                                      'packets_expected':packets_expected,
                                                                      'packets_missed':packets_missed,
                                                                      'snr': snr}]}
                    eval_exps.append(eval_exp_i)
                else:
                    print("Not creating new entry")
                    eval_exp_i = next((sub for sub in eval_exps if sub['exposure']==eval_exp))
                    voltages = eval_exp_i['voltages']
                    
                    if not any(voltage['voltage']==volt for voltage in voltages):
                        voltages.append({'voltage':volt,
                                         'ber':ber,
                                         'berm':ber,
                                         'packets_expected':packets_expected,
                                         'packets_missed':packets_missed,
                                         'snr': snr})
                    else:
                        continue
    
    with open(results_filename, 'w') as outfile:
        json.dump(results, outfile)
