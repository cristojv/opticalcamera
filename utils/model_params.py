# %%
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras import callbacks
import argparse
from kerastuner.tuners import Hyperband
from hyper_search import build_model
from datagenerator.generator import RSCameraGenerator
from acquire import acquire_ber, process_results
import time
from telecomms.telebot import send_message
import os.path
from gen import gen_dataset
import math

layers = 3
tuner_folder = '/media/cristojv/melon/tuner'
project_name = 'hyperoccen_num_layers_{}'.format(layers)
tuner = Hyperband(build_model,
                        objective="val_loss",
                        max_epochs=100,
                        factor=3,
                        hyperband_iterations=5,
                        directory = tuner_folder,
                        project_name = project_name)

tuner.reload()
best_hps = tuner.get_best_hyperparameters(1)[0]
print("Best hyperparameters: {}".format(best_hps.values))
model = tuner.hypermodel.build(best_hps)
# %%
