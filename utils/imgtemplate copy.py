import cv2
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from gen import gen_dataset, gen_sample
from instruments. waveform import manchester_encoding, upsample, add_footer, add_header
from gen import camera_filter

#%% PLOT Template matching for real captured images
%matplotlib inline
import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'

exposures = [85,104,123,142,161,180,198,217,236,255,274,293,
             312,331,350,369,387,406,425,444,463,482,
             501,520,539,558,577,595,614,633,652,671,690]
texp_trains = [161,
               236,
               312,
               444,
               520,
               652]

texp_evals_m = [exposures,
                exposures,
                exposures[0:-1],
                exposures[0:-1],
                exposures[0:-1],
                exposures[0:-1]]

# Estos valores están shifteados una unidad debido a que la cámara de la 
# rapi también lo hace
texp_real_m = [exposures,
               exposures,
               exposures[1:],
               exposures[1:],
               exposures[1:],
               exposures[1:],]

fig, ax = plt.subplots()
for idx,texp_train in enumerate(texp_trains):
    texp_evals = texp_evals_m[idx]
    max_auto = None
    max_vals = []
    width=10
    for idx1, texp_eval in enumerate(texp_evals):
        img = cv2.imread('/media/cristojv/melon/camera/analog_gain_5/{}us/180/_00000.jpg'.format(str(texp_eval).zfill(4)))
        img = (img[:,100:100+width,1]/255.0)**(1/2.4)
        tclk = 100e-9
        ts_ticks = 189
        chip_samples = 5
        tchip_ticks = ts_ticks * chip_samples
        texp_ticks = texp_train*10

        bits = np.asarray(list('011011010001101000010010001000100101000101'), dtype=np.int8)
        bits = add_header(bits, 5)
        bits = add_footer(bits, 1)
        bits = (bits - 1) * (-1) 
        template_shape = chip_samples * bits.shape[0]
        # print(template_shape)
        template = manchester_encoding(bits)

        signal_ticks = template.shape[0] * ts_ticks + texp_ticks

        template_upsampled = upsample(template, tchip_ticks)
        template_upsampled = np.repeat(np.expand_dims(template_upsampled,1), width, axis=1)

        template_filtered_no_gamma = camera_filter(template_upsampled,
                                        sig_shape=(template_shape,width),
                                        tclk=tclk,
                                        ts_ticks=ts_ticks,
                                        texp_ticks=texp_ticks,
                                        offset=0,
                                        gamma=None)

        img = np.asarray(img*255, dtype=np.uint8)
        template_filtered_no_gamma = np.asarray(template_filtered_no_gamma)
        template_filtered_no_gamma = (template_filtered_no_gamma - np.min(template_filtered_no_gamma)) / (np.max(template_filtered_no_gamma) - np.min(template_filtered_no_gamma))
        template_filtered_no_gamma = np.asarray(template_filtered_no_gamma*255, dtype=np.uint8)
        # # Correlation
        res = cv2.matchTemplate(img, template_filtered_no_gamma, cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        max_vals.append(max_val)
        if texp_train == texp_real_m[idx][idx1]:
            max_auto = max_val
    
    plt.plot(texp_real_m[idx],max_vals, 'o--', label=(int(texp_train)))
    markerline, stemlines, baseline = plt.stem([texp_train],[max_auto],linefmt='-.')
    plt.setp(stemlines, color=(0.7,0.7,0.7), linewidth=2)
    plt.setp(markerline, color=(0.1,0.1,0.1), linewidth=2)
    plt.setp(baseline, color=(0.7,0.7,0.7), linewidth=2)

ax.set_ylim(0.3,1)
ax.legend(loc='upper right',ncol=2)
# ax.set_title("Real images vs Synthetic image comparison")
ax.set_ylabel("Pearson Correlation Coefficient")
ax.set_xlabel("Exposure time ($\mu s$)")
plt.show()
#%% PLOT examples
%matplotlib inline
import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'
from bwanalysis import snr_bw

exposures = [85,104,123,142,161,180,198,217,236,255,274,293,
             312,331,350,369,387,406,425,444,463,482,
             501,520,539,558,577,595,614,633,652,671,690]
texp_trains = [161,
               236,
               293,
               425,
               501,
               633]
               
# texp_trains = [236]

texp_evals_m = [[161],
               [236],
               [312],
               [444],
               [520],
               [652]]
               
# texp_evals_m = [[236]]
# Estos valores están shifteados una unidad debido a que la cámara de la 
# rapi también lo hace
texp_real_m = [[161],
               [236],
               [312],
               [444],
               [520],
               [652]]
# texp_real_m = [[236]]

for idx,texp_train in enumerate(texp_trains):
    texp_evals = texp_evals_m[idx]
    max_auto = None
    max_vals = []
    width=10

    fig = plt.figure(figsize=(4,7))
    ax = fig.subplots(1,5, sharex=True, sharey=True)
    for idx1, texp_eval in enumerate(texp_evals):
        img = cv2.imread('/media/cristojv/melon/camera/analog_gain_5/{}us/180/_00000.jpg'.format(str(texp_eval).zfill(4)))
        img = (img[:,100:100+width,1]/255.0)**(1/2.4)
        tclk = 100e-9
        ts_ticks = 189
        chip_samples = 5
        tchip_ticks = ts_ticks * chip_samples
        texp_ticks = texp_train*10

        bits = np.asarray(list('011011010001101000010010001000100101000101'), dtype=np.int8)
        bits = add_header(bits, 5)
        bits = add_footer(bits, 1)
        bits = (bits - 1) * (-1) 
        template_shape = chip_samples * bits.shape[0]
        # print(template_shape)
        template = manchester_encoding(bits)

        signal_ticks = template.shape[0] * ts_ticks + texp_ticks

        template_upsampled = upsample(template, tchip_ticks)
        template_upsampled = np.repeat(np.expand_dims(template_upsampled,1), width, axis=1)

        template_filtered_no_gamma_short_exp = camera_filter(template_upsampled,
                                        sig_shape=(template_shape,width),
                                        tclk=tclk,
                                        ts_ticks=ts_ticks,
                                        texp_ticks=1,
                                        offset=0,
                                        gamma=None)

        template_filtered_no_gamma = camera_filter(template_upsampled,
                                        sig_shape=(template_shape,width),
                                        tclk=tclk,
                                        ts_ticks=ts_ticks,
                                        texp_ticks=texp_ticks,
                                        offset=0,
                                        gamma=None)

        img = np.asarray(img*255, dtype=np.uint8)
        template_filtered_no_gamma = np.asarray(template_filtered_no_gamma)
        template_filtered_no_gamma = (template_filtered_no_gamma - np.min(template_filtered_no_gamma)) / (np.max(template_filtered_no_gamma) - np.min(template_filtered_no_gamma))
        template_filtered_no_gamma = np.asarray(template_filtered_no_gamma*255, dtype=np.uint8)
        # # Correlation
        res = cv2.matchTemplate(img, template_filtered_no_gamma, cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        max_vals.append(max_val)
        if texp_train == texp_real_m[idx][idx1]:
            max_auto = max_val
    
    noisy_template = template_filtered_no_gamma.copy()/255.0
    noisy_template = noisy_template - np.min(noisy_template) / (np.max(noisy_template) - np.min(noisy_template))

    signal = noisy_template.copy()*0.4
    signal = signal - np.mean(signal)
    signal_power = np.sum(signal*signal)

    noisy_template = noisy_template*0.4 + np.random.normal(loc=0, scale=0.1, size=template_filtered_no_gamma.shape)
    
    noise_power = np.sum(np.random.normal(loc=0, scale=0.1, size=template_filtered_no_gamma.shape)**2)

    img_sample = img[max_loc[1]:max_loc[1]+template_filtered_no_gamma.shape[0],:].copy()/255.0
    
    snr = signal_power/(noise_power)
    SNR = 10*np.log10(snr)

    power_signal, power_noise = snr_bw(img_sample[:,0], texp_eval, 5)
    snr_r = power_signal/power_noise
    SNR_r = 10*np.log10(snr_r)

    print("TEMPLATE snr: {}, SNR: {}".format(snr, SNR))
    print("REAL snr: {}, SNR: {}".format(snr_r, SNR_r))

    noisy_template = (noisy_template *2*np.std(img_sample)) + np.mean(img_sample)
    
    noisy_template_zscore = (noisy_template - np.mean(noisy_template))/np.std(noisy_template)
    img_sample_zscore = (img_sample - np.mean(img_sample)) / np.std(img_sample)
    
    ax[0].imshow(template_filtered_no_gamma[:120,:]/255.0, vmin=0, vmax=1)
    ax[1].imshow(noisy_template[:120,:], vmin=0, vmax=1)
    ax[2].imshow(img_sample[:120,:], vmin=0, vmax=1)
    ax[3].imshow(noisy_template_zscore[:120,:], vmin=np.min(img_sample)-2, vmax=np.max(img_sample)+2)

    ax[4].imshow(img_sample_zscore[:120,:], vmin=np.min(img_sample)-2, vmax=np.max(img_sample)+2)
    ax[0].axis('off')
    ax[1].axis('off')
    ax[2].axis('off')
    ax[3].axis('off')
    ax[4].axis('off')

    plt.figure(figsize=(4,7))
    plt.axis('off')
    plt.imshow(template_filtered_no_gamma_short_exp[:120])

    fig.subplots_adjust(wspace=0.002)
    plt.show()
# %%
x_sample = np.asarray(x_data)
print(x_sample.shape)
x_sample = (x_sample - np.min(x_sample)) / (np.max(x_sample) - np.min(x_sample))



scale_factor = 0.10
print(x_sample.shape)
x_sample = x_sample * scale_factor + (1-scale_factor)/2
noise_factor=0.01
print(x_sample.shape)
x_sample = x_sample[..., tf.newaxis]
img = img[..., tf.newaxis]
x_sample = x_sample + noise_factor * tf.random.normal(shape=x_sample.shape)

# %%
x_sample = tf.image.per_image_standardization(x_sample)
img = tf.image.per_image_standardization(img)
print(x_sample.shape)
plt.plot(x_sample[:,0,0])
plt.show()
plt.plot(img[0:256,0])
plt.show()
# %%
from acquire import generate_header_template
from waveform import manchester_encoding, upsample
from gen import camera_filter

template = np.ones(100)
template = np.insert(template, 0, 0)
template = manchester_encoding(template)
template = (template-1)*-1
plt.plot(template)
plt.show()
template.shape

texp = 444
tclk = 100e-9
ts_ticks = 189
chip_samples = 5
tchip_ticks = ts_ticks * chip_samples
texp_ticks = texp*10

signal_ticks = template.shape[0] * ts_ticks + texp_ticks

template_upsampled = upsample(template, tchip_ticks)
template_upsampled = np.repeat(np.expand_dims(template_upsampled,1), 16, axis=1)
plt.plot(template_upsampled[:,0])
plt.show()
print(template_upsampled.shape)
template_filtered_no_gamma = camera_filter(template_upsampled,
                                  sig_shape=template.shape,
                                  tclk=tclk,
                                  ts_ticks=ts_ticks,
                                  texp_ticks=texp_ticks,
                                  offset=0,
                                  gamma=None)
template_filtered_gamma = camera_filter(template_upsampled,
                                  sig_shape=template.shape,
                                  tclk=tclk,
                                  ts_ticks=ts_ticks,
                                  texp_ticks=texp_ticks,
                                  offset=0,
                                  gamma=2.4)

template_filtered_no_gamma = np.asarray(template_filtered_no_gamma)
template_filtered_gamma = np.asarray(template_filtered_gamma)

plt.plot(template_filtered_no_gamma[:,0])
plt.plot(template_filtered_gamma[:,0])
plt.show()
# %%
