#%%
%load_ext autoreload
%autoreload 2
#%%
import cv2
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from gen import gen_dataset, gen_sample
from waveform import manchester_encoding, upsample, add_footer, add_header
from gen import camera_filter
from bwanalysis import snr_equalization

%matplotlib inline
import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'

texp_evals = [142,293,
               425,
               501]

texp_exposures = [161,312,444,520]
one=False
for idx,texp_eval in enumerate(texp_evals):

    for voltage in [180,130]:
        width=1
        img = cv2.imread('/media/cristojv/melon/camera/analog_gain_5/{}us/{}/_00000.jpg'.format(str(texp_eval).zfill(4),voltage))
        img_noise = img.copy()
        img_noise = (img_noise[:,:,1]/255.0)
        img = (img[:,960:960+width,1]/255.0)
        tclk = 100e-9
        ts_ticks = 189
        chip_samples = 5
        tchip_ticks = ts_ticks * chip_samples
        texp_ticks = texp_exposures[idx]*10


        bits = np.asarray(list('011011010001101000010010001000100101000101'), dtype=np.int8)
        bits = add_header(bits, 5)
        bits = add_footer(bits, 1)
        bits = (bits - 1) * (-1) 
        template_shape = chip_samples * bits.shape[0]
        # print(template_shape)
        template = manchester_encoding(bits)

        signal_ticks = template.shape[0] * ts_ticks + texp_ticks

        template_upsampled = upsample(template, tchip_ticks)
        template_upsampled = np.repeat(np.expand_dims(template_upsampled,1), width, axis=1)

        template_filtered_no_gamma = camera_filter(template_upsampled,
                                        sig_shape=(template_shape,width),
                                        tclk=tclk,
                                        ts_ticks=ts_ticks,
                                        texp_ticks=texp_ticks,
                                        offset=0,
                                        gamma=None)

        img = np.asarray(img*255, dtype=np.uint8)
        template_filtered_no_gamma = np.asarray(template_filtered_no_gamma)
        template_filtered_no_gamma = (template_filtered_no_gamma - np.min(template_filtered_no_gamma)) / (np.max(template_filtered_no_gamma) - np.min(template_filtered_no_gamma))
        template_filtered_no_gamma = np.asarray(template_filtered_no_gamma*255, dtype=np.uint8)
        # # Correlations
        res = cv2.matchTemplate(img, template_filtered_no_gamma, cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        
        # print(max_val)
        # print(max_loc)
        # print(template.shape)
        img = img/255.0


        print(texp_eval, texp_exposures[idx])
        psignal1, pnoise1 = snr_equalization(img_noise, texp_exposures[idx], 5)
        # psignal1, pnoise1 = snr_bw(img[:,0], texp_eval, 5)
        snr1 = psignal1/pnoise1
        snr1 = snr1 if snr1 > 0 else 1e-1
        snr1 = 10 * np.log10(snr1)-4.4

        snr2 = 20 * np.log10(np.mean(img_noise)/np.std(img_noise))
        print("SNR1: {}, SNR2: {}".format(snr1,snr2))

        if not one:


            one=True
            plt.plot(img[max_loc[1]:max_loc[1]+template_filtered_no_gamma.shape[0],0],
                     color=(248/255,149/255,64/255),
                     label='Noisy captured signal with SNR = {0:0.1f} dB'.format(snr1))
            plt.plot((template_filtered_no_gamma[:,0]/255.0*2-1)*np.std(img)*2+0.13,
                     color=(0.1,0.1,0.1),
                     linestyle='--',
                     label='Reference (No scaled), $t_{exp}$'+'$={}$ $\mu s$'.format(texp_exposures[idx]))
            # plt.plot(np.mean(img_noise, axis=1))
        else:
            one=False
            plt.plot(img[max_loc[1]:max_loc[1]+template_filtered_no_gamma.shape[0],0],
                     color=(181/255,47/255,140/255),
                     label='Noisy captured signal with SNR = {0:0.1f} dB'.format(snr1))
            # plt.plot((template_filtered_no_gamma[:,0]/255.0*2-1)*np.std(img)*2.0+np.mean(img),
            #          linestyle='-',
            #          color=(0.1,0.1,0.1))
            # plt.plot(np.mean(img_noise, axis=1))
    plt.ylim(0,0.5)
    plt.xlabel('pixel sample')
    plt.ylabel('pixel value')
    plt.legend()
    plt.show()
    pass
# %%
