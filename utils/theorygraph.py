# %%
%matplotlib inline
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'
import mpl_toolkits.mplot3d.art3d as art3d
from mpl_toolkits.mplot3d.axes3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
import numpy as np
import math

amp = 1
ftx = 1000
fs = 200000
fcs = 3000

Ttx = 1/ftx
Ts = 1/fs
Tcs = 1/fcs

fig = plt.figure(figsize=(8,5))
ax = fig.add_subplot(111,projection='3d')

yline = np.arange(fs)
xline = np.ones(yline.shape[0])

zline = (np.sin(2*np.pi*ftx * (yline*Ts))+1)/2
qzline = zline.copy()
qzline[qzline>0.5] = 1
qzline[qzline<0.5] = 0

# zline = qzline

tend = Ttx*4
tend_idx = int(tend*fs)
ttcs=int(Tcs*fs) # Number of samples per Tcs
for x in range(0,10):
    ax.plot(xline[0:tend_idx]*x,
            yline[0:tend_idx]/fs,
            (-x+10)**2*zline[0:tend_idx]/(10)**2,
            c=plt.get_cmap("plasma")((10-x)/12),
            linestyle='-')

x=np.arange(10)
y=x*Tcs
z=(-x+10)**2*(np.sin(2*np.pi*ftx * y)+1)/2/(10)**2

# for xi, yi, zi in zip(x, y, z): 
#     line=art3d.Line3D(*zip((xi, yi, 0), (xi, yi, zi)), marker='o', markevery=(1, 1), color='black')
#     label = 'v[m={}]'.format(xi)
#     # ax.text(xi, yi, zi+0.2, label)
#     ax.add_line(line)

# ax.plot(x,y,z, c='black', linestyle='--')

# ax.plot(x,np.ones(y.shape[0])*13*Tcs,z, c='black', linestyle='--')

# y = np.ones(y.shape[0])*13*Tcs
# for xi, yi, zi in zip(x, y, z): 
#     line=art3d.Line3D(*zip((xi, yi, 0), (xi, yi, zi)), marker='o', markevery=(1, 1), color='black')
#     label = 'v[m={}]'.format(xi)
#     # if xi%2 == 0:
#         # ax.text(xi, yi+Tcs, zi, label)
#     ax.add_line(line)

ax.axes.set_xlim(left=0, right=10)
ax.set_xticks(np.arange(10))
ax.axes.set_ylim3d(bottom=0, top=tend)
tend_idx
ax.set_yticks(np.arange(tend_idx/ttcs+1)*Tcs)
# ax.axes.set_ylim3d(bottom=0, top=tend)

def formatting_ts(y, p):
    if p%2==0:
        return "{0:0.0f}Ts".format(p) if p>1 else "Ts" if p==1 else "0"
    else:
        return ''

ax.get_yaxis().set_major_formatter(
                            matplotlib.ticker.FuncFormatter(formatting_ts))
ax.axes.set_zlim3d(bottom=0, top=1) 
ax.view_init(elev=52, azim=-15)
ax.set_xlabel('n')
ax.set_ylabel('t')
ax.set_zlabel('v')
ax.get_proj = lambda: np.dot(Axes3D.get_proj(ax), np.diag([1, 1.20, 1, 1]))
ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))

# ax.xaxis._axinfo["grid"]['color'] =  (1,1,1,0)
# ax.yaxis._axinfo["grid"]['color'] =  (1,1,1,0)
# ax.grid(b=None)
ax.zaxis._axinfo["grid"]['color'] =  (1,1,1,0)
# %%
