# %% TensorFlow 2 quick start for beginners
# https://www.tensorflow.org/tutorials/quickstart/beginner
import tensorflow as tf

# Load and prepare the MNIST dataset
mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

# Build the Sequential model by stacking layers. Choose and optimizer and
# loss function for training.

model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(10)
])

# For each example the model returns a vector of "logits" or "log-odds" scores,
# one for each class

predictions = model(x_train[:1]).numpy()

# The softmax normalization function converts these logits to "probabilities"
# for each class

tf.nn.softmax(predictions).numpy()

# The losses. SparseCategoricalCrossentropy loss takes a vector of logics and a
# True index and returns a scalar loss for each example

loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

# This loss is equal to the negative log probability of the true class: It is
# zero if the model is sure of the correct class.
# The untrained model gives probabilites close to random (1/10 for each class),
# so the initial loss should be close to -tf.math.log(1/10) ~= 2.3.

loss_fn(y_train[:1], predictions).numpy()

model.compile(optimizer='adam', loss=loss_fn, metrics=['accuracy',)

# The Model.fit method adjusts the model parameters to minimize the loss
# Epoch:    represents when an ENTIRE dataset is passed forward and backward
#           through the neural network only ONCE. The dataset is divided in
#           batchs
# Batch:    total number of training examples for each division of the dataset
# Iterations: The number of batches needed to complete one epoch.

model.fit(x_train, y_train, epochs=5)

# The Model.evaluate method checks the models performance, usually on a
# "Validation-set" of "Test-set".

model.evaluate(x_test, y_test, verbose=2)

# If you want your model to return a probability, you can wrap the trained
# model, and attach the softmax to it:

probability_model = tf.keras.Sequential([
    model,
    tf.keras.layers.Softmax()
])

probability_model(x_test[:5])
# %% TensorFlow 2 quickstart for experts
import tensorflow as tf

from tensorflow.keras.layers import Dense, Flatten, Conv2D
from tensorflow.keras import Model

mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

# Add a channels dimension
x_train = x_train[..., tf.newaxis].astype("float32")
x_test = x_test[..., tf.newaxis].astype("float32")

# Use tf.data to catch and shuffle the dataset
train_ds = tf.data.Dataset.from_tensor_slices((x_train, y_train))\
           .shuffle(10000)\
           .batch(32)
test_ds = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(32)

# Build the tf.keras model using the Keras model subclassing API

class MyModel(Model):
    def __init__(self):
        super(MyModel, self).__init__()
        self.conv1 = Conv2D(filters=32, kernel_size=3,activation='relu')
        self.flatten = Flatten()
        self.d1 = Dense(units=128, activation='relu')
        self.d2 = Dense(units=10)

    def call(self, x):
        x = self.conv1(x)
        x = self.flatten(x)
        x = self.d1(x)
        return self.d2(x)

# Create an instance of the model
model = MyModel()

# Choose an optimizer and loss function for training:
loss_obj = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
optimizer = tf.keras.optimizers.Adam()

# Select metrics to measure the loss and the accuracy of the model. These
# metrics accumulate the values over epochs and then print the overal
# result.

train_loss = tf.keras.metrics.Mean(name='train_loss')
train_accuracy = tf.keras.metrics.SparseCategoricalCrossentropy(name='train_accuracy')

test_loss = tf.keras.metrics.Mean(name='test_loss')
test_accuracy = tf.keras.metrics.SparseCategoricalCrossentropy(name='test_accuracy')

# Use tf.GradientTape to train the model
@tf.function
def train_step(images, labels):
    with tf.GradientTape() as tape:
        # trining=True is only needed if there are layers with different
        # behaviour during training versus inference (e.g. Dropout)

        predictions = model(images, training=True)
        loss = loss_obj(labels, predictions)
    gradients = tape.gradient(loss, model.trainable_variables)
    optimizer.apply_gradients(zip(gradients, model.trainable_variables))

    train_loss(loss)
    train_accuracy(labels, predictions)

# Test the model
@tf.function
def test_step(images, labels):
    # training=False is only needed if there are layers with different
    # behaviour during training versus inferences (e.g. Dropout).

    predictions = model(images, training=False)
    t_loss = loss_obj(labels, predictions)

    test_loss(t_loss)
    test_accuracy(labels, predictions)

EPOCHS = 5

for epoch in range (EPOCHS):
    # Reset the metrics at the start of the next epoch
    train_loss.reset_states()
    test_loss.reset_states()
    train_accuracy.reset_states()
    test_accuracy.reset_states()

    for images, labels in train_ds:
        train_step(images, labels)
    
    for test_images, test_labels in test_ds:
        test_step(test_images, test_labels)
    
    print(
        f'Epoch {epoch + 1}, '
        f'Loss: {train_loss.result()}, '
        f'Accuracy: {train_accuracy.result() * 100}, '
        f'Test Loss: {test_loss.result()}, '
        f'Test Accuracy: {test_accuracy.result() * 100}'
    )
# %% Importing Numpy dataset
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

from tensorflow.keras.layers import Dense, Flatten, Conv2D
from tensorflow.keras import layers, regularizers, losses, Model, History

latent_dim = 500

class Autoencoder(Model):
    def __init__(self, latent_dim):
        super(Autoencoder, self).__init__()
        self.latent_dim = latent_dim
        self.encoder = tf.keras.Sequential([
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(latent_dim, activation='sigmoid',bias_initializer='uniform')
        ])
        self.decoder = tf.keras.Sequential([
            tf.keras.layers.Dense(300, activation='tanh',bias_initializer='uniform')
        ])
    
    def call(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded

class ConvAutoencoder(Model):
    def __init__(self):
        super(ConvAutoencoder, self).__init__()
        self.encoder = tf.keras.Sequential([
            layers.Input(shape=(64, 12, 1)),

            layers.Conv2D(16, (3,3),
                          activation='relu',
                          padding='same', strides=1,
                          kernel_initializer='random_normal'),
            layers.MaxPool2D(pool_size=(2,2)),
            layers.Conv2D(8, (3,3),
                          activation='relu',
                          padding='same',
                          strides=1,
                          kernel_initializer='random_normal'),
                        #   kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))
            layers.MaxPool2D(pool_size=(2,2)),
        ])
        self.decoder = tf.keras.Sequential([
            layers.Input(shape=np.asarray(self.encoder.output_shape)[1:]),
            layers.Conv2DTranspose(8, kernel_size=3, strides=1, activation='relu', padding='same', kernel_initializer='random_normal',kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),),
            layers.UpSampling2D(size=(2,2)),
            layers.Conv2DTranspose(16, kernel_size=3, strides=1, activation='relu', padding='same', kernel_initializer='random_normal',kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),),
            layers.UpSampling2D(size=(2,2)),
            layers.Conv2D(1, kernel_size=(3,3), activation='sigmoid', padding='same', kernel_initializer='random_normal',kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))
        ])
    
    def call(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded
        
# autoencoder = Autoencoder(latent_dim)
autoencoder = ConvAutoencoder()

# def loss_custom(y_true, y_pred):
#     d = y_true - y_pred
#     tf.print("\n y_true:", type(y_true), output_stream=sys.stdout)
#     return tf.reduce_mean(tf.square(d))

with np.load('./dataset-caca.npz') as data:
    x_train = data['x_train'].astype('float32')
    x_test = data['x_test'].astype('float32')
    y_train = data['y_train'].astype('float32')
    y_test = data['y_test'].astype('float32')
    x_train = x_train[..., tf.newaxis]
    x_test = x_test[..., tf.newaxis]
    y_train = y_train[..., tf.newaxis]
    y_test = y_test[..., tf.newaxis]

    train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
    test_dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
    
    BATCH_SIZE = 100
    SHUFFLE_BUFFER_SIZE = 100

    train_dataset = train_dataset.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
    test_dataset = test_dataset.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)

    autoencoder.compile(optimizer='adam', loss=losses.MeanSquaredError())
    autoencoder.fit(train_dataset, epochs=4, validation_data=test_dataset)
# %%
encoded_imgs = autoencoder.encoder(x_test).numpy()
decoded_imgs = autoencoder.decoder(encoded_imgs).numpy()

# plt.plot(x_test[0], 'green')
# plt.imshow(y_test[0])
# plt.imshow(encoded_imgs)
plt.imshow(x_test[2])
plt.figure()
plt.imshow(y_test[2])
plt.figure()
plt.imshow(decoded_imgs[2])
# %%
layer = tf.keras.layers.experimental.preprocessing.Normalization()
layer.adapt(x_test[0])
plt.plot(layer(x_test[0]))
