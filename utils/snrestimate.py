# %%
import numpy as np
import os
import collections
import cv2
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
#%%
INPUT_FOLDER = './realphotoexamples/snr'

Sample = collections.namedtuple('Sample', 'volt snr exp')
off_samples = []

for filename in os.listdir(INPUT_FOLDER):
    if filename.endswith('off.jpg'):
        exp = int(filename[0:5])
        
        img = cv2.imread(INPUT_FOLDER+'/'+filename)
        img = img[400:600, 750:1000, 1]/255.0
        img = img **(1.8) #Gamma
        std = img.std()
        mean = img.mean()
        off_samples.append([exp, mean, std])
off_samples = np.asarray(off_samples)
samples = []
for filename in os.listdir(INPUT_FOLDER):
    # Get values from name
    if filename.endswith('on.jpg'):
        exp = int(filename[0:5])
        volt = float(filename[13:16])/10+18
        print(exp, volt)
        img = cv2.imread(INPUT_FOLDER+'/'+filename)
        img = img[400:600, 750:1000, 1]/255.0
        img = img **(1.8) #Gamma
        std = img.std()
        mean = img.mean()

        # Get off mean
        idx = np.where(off_samples[:,0]==exp)[0]

        mean_d = (mean + off_samples[idx,1])/2
        snr = 10 * np.log10(mean**2 * 189/exp/std**2)
        samples.append([exp, snr, volt, mean, off_samples[idx,1], std])
samples = np.asarray(samples)
np.save('./tmp/snr/samples-bw.npy',samples)
# %%
samples = np.load('./tmp/snr/samples-bw.npy', allow_pickle=True)
samples1 = np.load('./tmp/snr/samples.npy', allow_pickle=True)
# %%
exps = np.asarray(list(np.unique(samples[:,0])))
exps = exps[np.linspace(1,len(exps)-1,10).astype(np.int)]
fs = []
plots = []
for exp in exps:
    idx = np.where(samples[:,0]==exp)
    plt.scatter(*samples[idx,1],*samples[idx,2], label='{}us'.format(exp))
    fs.append([exp, interp1d(*samples[idx,1],*samples[idx,2],kind='linear')])

plt.title('SNR vs source voltage')
plt.ylabel('Voltage (V)')
plt.xlabel('SNR (dB)')
plt.legend(scatterpoints=1, loc='lower right', ncol=2)
snr = 19
exps = []
volts = []
for f in fs:
    try:
        volt = f[1](snr)-18
        plt.scatter(snr, f[1](snr))
        print("Exposure: {}, Voltage: {:0.3f}".format(f[0], volt))
        exps.append(f[0])
        volts.append(volt)
    except ValueError:
        print("Error")
plt.show()
print(' '.join([str(i) for i in exps]))
print(' '.join([str(round(i, 3)) for i in volts]))
# %%
exps = np.asarray(list(np.unique(samples1[:,0])))
exps = exps[np.linspace(1,len(exps)-1,10).astype(np.int)]
fs = []
plots = []
for exp in exps:
    idx = np.where(samples1[:,0]==exp)
    plt.scatter(*samples1[idx,1],*samples1[idx,2], label='{}us'.format(exp))
    fs.append([exp, interp1d(*samples1[idx,1],*samples1[idx,2],kind='linear')])

plt.title('SNR vs source voltage')
plt.ylabel('Voltage (V)')
plt.xlabel('SNR (dB)')
plt.legend(scatterpoints=1, loc='lower right', ncol=2)
snr = 19
exps = []
volts = []
for f in fs:
    try:
        volt = f[1](snr)-18
        plt.scatter(snr, f[1](snr))
        print("Exposure: {}, Voltage: {:0.3f}".format(f[0], volt))
        exps.append(f[0])
        volts.append(volt)
    except ValueError:
        print("Error")
plt.show()
print(' '.join([str(i) for i in exps]))
print(' '.join([str(round(i, 3)) for i in volts]))
# %%
np.savetxt("./realphotoexamples/snr/snr/exposures.csv", [exps], delimiter=",")
np.savetxt("./realphotoexamples/snr/snr/voltages19.csv", volts, delimiter=",")
# %%
