import os.path
import os
import numpy as np
from acquire import process_results

if __name__ == '__main__':

    folder = '/media/cjurado/evaluation/hyperoccen_num_layers_2'
    for filename in os.listdir(folder):
        if filename.startswith('res'):
            results = np.genfromtxt('{}/{}'.format(folder, filename), delimiter=',')
            ber, berm, packets_expected, packets_missed, packets_received, packets_valid = process_results(results)
            # print(ber, berm, packets_expected, packets_received, packets_valid)
            
            summary_filename = list(filename)
            summary_filename[0] = 's'
            summary_filename[1] = 'u'
            summary_filename[2] = 'm'
            summary_filename = ''.join(summary_filename)
            print(summary_filename)
            np.savetxt('{}/{}'.format(folder,summary_filename),
                    np.asarray([ber, berm, packets_expected, packets_missed, packets_received, packets_valid]),
                    delimiter=',', fmt='%s')
