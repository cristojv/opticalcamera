# %%
import numpy as np
import matplotlib.pyplot as plt
import cv2
import tensorflow as tf
import time
import argparse
from hyper_search import reload_best_models, build_model
from waveform import manchester_encoding, upsample
from scipy.signal import find_peaks
from enum import Enum

model_folder = '/media/cristojv/melon/models/hyperoccen_num_layers_3/training_exp_0444us'
model = tf.keras.models.load_model(model_folder)
chip_length = 5
thr_level = 0.6
width = 16
debug_level=0
# Load validation images
# reader = cv2.VideoCapture('{}/_%05d.jpg'.format(input_folder))
# number_of_bits = expected_bits.shape[1]
# print(number_of_bits)

# Start
img1 = cv2.imread('./tmp/example/_00000.jpg')
gamma = 2.4
img = (img1[:, :, 1]/255.0)**(1/gamma)

# MEAN
# img = (img[:,:,1]/255.0)**(1/gamma)
# img = np.mean(img, axis=1)
# img = np.repeat(np.expand_dims(img, axis=1),width,axis=1)

# NO-MEAN
# start=960

result_img = np.ones(img.shape)

isHappen = False
for start in np.arange(0,img.shape[1],16):
    # Get all the images sections
    part = img[:, start:start+width]
    # plt.imshow(part)
    # plt.show()
    images = []
    number_of_segments = 6
    valid_pixels = 1080/number_of_segments
    overlapping = 256 - valid_pixels
    overlapping_top = int(overlapping//2)
    overlapping_bottom = overlapping_top
    segments = []

    if debug_level >=3 and not isHappen:
        isHappen=True
        print("Number of segments: {}".format(number_of_segments))
        print("Valid segment length: {}".format(valid_pixels))
        print("Total overlapping pixels: {}".format(overlapping))
        print("Top overlapping pixels: {}".format(overlapping_top))
        print("Bottom overlapping pixels: {}".format(overlapping_bottom))

        idx0 = (0,256)
        idx1 = (216-overlapping_top)
        idx2 = (216*2-overlapping_top)
    if debug_level >= 3:
        img_rectangles = part.copy()
    for i in range(number_of_segments):
        if (i == 0):
            top=0
        elif (i == number_of_segments-1):
            top=1080-256
        else:
            top = valid_pixels*i - overlapping_top
        
        top = int(top)
        bot = top+256
        # print("TOP: {}, BOT: {}".format(top, bot))
        segment = slice(top, bot)
        segments.append(segment)
        img_s = part[segment]
        images.append(img_s)

        if debug_level >= 3:
            img_rectangles=cv2.rectangle(img_rectangles, (0,top), (int(width),bot), (255,255,255), 3)
    
    if debug_level >= 3:
        plt.imshow(img_rectangles)
        plt.show()
    images = np.asarray(images)
    images = images[..., tf.newaxis]

    images = tf.image.per_image_standardization(images)
    if debug_level == 3:
        plt.plot(images[0,:,0])
        plt.show()
    real_encoded_imgs = model.encoder(images).numpy()
    real_decoded_imgs = model.decoder(real_encoded_imgs).numpy()
    
    fusion = np.ones((256,16))
    fusion_top = fusion.copy()
    fusion_bottom = fusion.copy()
    step = int(overlapping_top)
    fusion_top[0:step,:] =\
        np.repeat(
            np.expand_dims(
                np.arange(1,step+1)/step,
                axis=1),
            16,axis=1)
    fusion_bottom[-step:,:] =\
        np.repeat(
            np.expand_dims(
                np.arange(step-1,-1,-1)/step,
                axis=1),
            16,axis=1)

    fusion_top[fusion_top<1]=0
    fusion_bottom[fusion_bottom<1]=0
    img_g = np.zeros((1080,16))
    img_g_0 = np.squeeze(real_decoded_imgs[0])
    img_g_0 = img_g_0[:256-overlapping_bottom]
    fusion_0 = fusion_bottom[overlapping_bottom:]

    # if debug_level>=3:
    #     print(img_g_0.shape)
    #     print(fusion_0.shape)
    #     plt.imshow(fusion_top)
    #     plt.show()
    #     plt.imshow(fusion_bottom)
    #     plt.show()
    #     plt.imshow(fusion_top*fusion_bottom)
    #     plt.show()

    img_g[:256-overlapping_top,:] = img_g_0*fusion_0
    
    if debug_level>=3:
        plt.figure()
        plt.imshow(img_g)

    for i in range(1,number_of_segments-1):
        top = int(i*valid_pixels-overlapping_top)
        img_g[top:top+256,:] =\
            img_g[top:top+256,:] +\
            np.squeeze(real_decoded_imgs[i])* fusion_top * fusion_bottom
        
        # if debug_level >= 3:
        #     plt.figure()
        #     plt.imshow(img_g)

    img_g_5=np.squeeze(real_decoded_imgs[-1])
    img_g_5 = img_g_5[overlapping_top:]
    fusion_5 = fusion_top[:256-overlapping_top]
    img_g[-256+overlapping_top:,:] = img_g[-256+overlapping_top:,:] + img_g_5*fusion_5
    # img_g = np.clip(img_g,0,1)

    # if debug_level >= 3:
    #     plt.figure()
    #     plt.imshow(img_g_5)
    #     plt.figure()
    #     plt.imshow(real_decoded_imgs[-1])
    #     plt.figure()
    #     plt.imshow(img_g_5*fusion_5)
    #     plt.imshow(img_g)
    #     plt.show()
    # img_m = np.expand_dims(np.mean(img_g, axis=1),axis=1)

    # img_m = img_m.astype(np.float32)
    # img_m = (img_m - np.mean(img_m))/np.std(img_m)


    # if debug_level >= 3:
    #     plt.figure()
    #     img_debug = img_g.copy()
    #     img_debug = np.expand_dims(img_debug, axis = 2)
    #     img_debug = np.repeat(img_debug,10, axis=1)
    #     www = img_debug.shape[1]
    #     img_debug = np.repeat(img_debug,3, axis=2)
    #     img_debug[:,:,0]=0
    #     img_debug[:,:,2]=0
    #     print(img_debug.shape)
        
    #     for i in range(number_of_segments):
    #         if (i == 0):
    #             top_segment=0
    #         elif (i == number_of_segments-1):
    #             top_segment=1080-256
    #         else:
    #             top_segment = valid_pixels*i - overlapping_top
            
    #         top_segment = int(top_segment)
    #         bot_segment = top_segment+256
    #         # print("TOP: {}, BOT: {}".format(top_segment, bot_segment))

    #         img_debug=cv2.rectangle(img_debug, (0,top_segment), (www,bot_segment), (1,0,0), 3)
    #     img_debug = cv2.rectangle(img_debug,(0,top-template.shape[0]),(www,bot), color=(0,0,1), thickness=5)
    #     plt.imshow(img_debug)
    #     plt.title("Image generated with AI")
    #     plt.figure()
    #     plt.imshow(img_g[top:bot, 0:16])
    #     plt.show()
    # plt.figure()
    # plt.plot(img_m)
    # img_o = img.copy()
    # plt.plot(img_o[:,0])
    # plt.title("Original image")
    # plt.show()

    result_img[:, start:start+width] = img_g

s, thr_img = cv2.threshold(result_img*255,127,255,cv2.THRESH_BINARY)
s, thr_img = cv2.threshold(thr_img,125,255,cv2.THRESH_BINARY)
plt.imshow(result_img[0:600,500:650])
plt.axis('off')
plt.show()
plt.imshow((img[0:600,500:650]-np.mean(img[0:600,500:650]))/np.std(img[0:600,500:650]))
plt.axis('off')
plt.show()
plt.imshow(img1[0:600,500:650])
plt.axis('off')
plt.show()
# %%
