# %% PLOT BER vs Exposure Time
%matplotlib inline
import numpy as np
import json
import matplotlib.pyplot as plt
import os
import matplotlib
from numpy.core.fromnumeric import transpose
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'
layers=2
plot_exposures = [161, 236, 312, 444, 520, 633]
exposures_list = [85, 104, 123, 142, 161, 180, 198, 217, 236, 255, 274, 293, 312, 331, 350, 369, 387, 406, 425, 444, 463, 482, 501, 520, 539, 558, 577, 595, 614, 633, 652, 690]
if __name__ == '__main__':
    tx_time = 18.9*5
    ber_filename = '/media/cristojv/melon/evaluation/analog_gain_5/column 960/hyperoccen_num_layers_{}/ber.json'.format(layers)

    if os.path.exists(ber_filename):
        with open(ber_filename) as json_file:
            ber = json.load(json_file)

            fig, ax = plt.subplots()
            for training_exp in ber:

                train_exp = int(training_exp['train_exp'])
                if train_exp in plot_exposures:
                    # if layers==2:
                    #     plt.title("Two levels topology")
                    # if layers==3:
                    #     plt.title("Three levels topology")

                    exposures = []
                    bers = []
                    for eval_exp_i in training_exp['eval_exposures']:
                        exposure = int(eval_exp_i['exposure'])
                        if train_exp>312:
                            if exposures_list.index(exposure) + 1 < len(exposures_list):
                                exposure = exposures_list[exposures_list.index(exposure) + 1]
                            else:
                                continue
                        for voltage in eval_exp_i['voltages']:
                            if int(voltage['voltage']) == 360:
                                exposures.append(exposure)
                                ber = float(voltage['berm'])
                                ber = ber if ber > 0 else 9e-6
                                # ber = ber if ber < 0.5 else 0.5
                                bers.append(ber)
                    
                    values = np.append(np.expand_dims(exposures,axis=1), np.expand_dims(bers,axis=1), axis=1)
                    values = values[values[:,0].argsort()]
                    ax.plot(values[:,0], values[:,1], '--o',label=int(training_exp['train_exp']))
                    # ax.set_ylim(5*1e-6,1)
                    # ax.set_xlim(50,700,50)
                    ax.set_yscale('log')
            ax.get_xaxis().set_major_formatter(
                            matplotlib.ticker.FuncFormatter(lambda x, p: "{0:0.0f}\n[{1:0.1f}]".format(x, x/tx_time)))
            handles, labels = ax.get_legend_handles_labels()
            # sort both labels and handles by labels
            labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
            ax.axhline(3.8*1e-3, color='black', linestyle='--')
            ax.text(540,(3.8*1e-3)+0.001,'FEC limit = $3.8x10^{-3}$',rotation=0,weight='bold')
            ax.legend(handles, labels, title='$T_\mathrm{exp}^\mathrm{TRAIN}$')
            ax.xaxis.set_major_locator(plt.MaxNLocator(10))
            ax.set_ylabel('BER')
            ax.set_xlabel('Exposure time ($\mu s$) [Ratio]')
            plt.grid(True, 'major', 'y', linestyle='--', linewidth=0.5)
            plt.grid(True, 'minor', 'y', linestyle='--', linewidth=0.3)

# %% PLOT BER vs SNR
import numpy as np
import json
import matplotlib.pyplot as plt
import os
import matplotlib
import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'stix'

matplotlib.rcParams['font.family'] = 'STIXGeneral'

layers=2
plot_exposures = [161, 236, 312, 444, 520]
voltages_array = np.asarray([130,140,150,160,170,180])+180
if __name__ == '__main__':
    tx_time = 18.9*5
    ber_filename = '/media/cristojv/melon/evaluation/analog_gain_5/column 960/hyperoccen_num_layers_{}/ber.json'.format(layers)
    if os.path.exists(ber_filename):
        with open(ber_filename) as json_file:
            ber = json.load(json_file)

            fig, ax = plt.subplots()
            for training_exp in ber:
                # if layers==2:
                #     plt.title("Two levels topology")
                # if layers==3:
                #     plt.title("Three levels topology")

                voltages = []
                bers = []
                
                train_exp = int(training_exp['train_exp'])
                if train_exp in plot_exposures:
                    for eval_exp_i in training_exp['eval_exposures']:
                        
                        exposure = int(eval_exp_i['exposure'])
                        if train_exp>=100:
                            if train_exp == 312 and exposure == 293:
                                for voltage in eval_exp_i['voltages']:
                                    if int(voltage['voltage']) in voltages_array:
                                        snr = int(voltage['snr'])-3
                                        # snr = snr if snr > 0 else 0
                                        voltages.append(snr)
                                        ber = float(voltage['berm'])
                                        ber = ber if ber > 0 else 9e-6
                                        bers.append(ber)
                            if train_exp == 444 and exposure == 425:
                                for voltage in eval_exp_i['voltages']:
                                    if int(voltage['voltage']) in voltages_array:
                                        snr = int(voltage['snr'])-3
                                        # snr = snr if snr > 0 else 0
                                        voltages.append(snr)
                                        ber = float(voltage['berm'])
                                        ber = ber if ber > 0 else 9e-6
                                        bers.append(ber)
                            elif train_exp == 520 and exposure == 501:
                                for voltage in eval_exp_i['voltages']:
                                    if int(voltage['voltage']) in voltages_array:
                                        snr = int(voltage['snr'])-3
                                        # snr = snr if snr > 0 else 0
                                        voltages.append(snr)
                                        ber = float(voltage['berm'])
                                        ber = ber if ber > 0 else 9e-6
                                        bers.append(ber)
                            elif train_exp == 161 and exposure == 142:
                                for voltage in eval_exp_i['voltages']:
                                    if int(voltage['voltage']) in voltages_array:
                                        snr = int(voltage['snr'])-3
                                        # snr = snr if snr > 0 else 0
                                        voltages.append(snr)
                                        ber = float(voltage['berm'])
                                        ber = ber if ber > 0 else 9e-6
                                        bers.append(ber)
                        else:  
                            if exposure == train_exp:
                                for voltage in eval_exp_i['voltages']:
                                    if int(voltage['voltage']) in voltages_array:
                                        snr = int(voltage['snr'])-3
                                        # snr = snr if snr > 0 else 0
                                        voltages.append(snr)
                                        ber = float(voltage['berm'])
                                        ber = ber if ber > 0 else 9e-6
                                        bers.append(ber)
                    values = np.append(np.expand_dims(voltages,axis=1), np.expand_dims(bers,axis=1), axis=1)
                    values = values[values[:,0].argsort()]
                    ax.plot(values[:,0], values[:,1], '--o',label=train_exp)
                    ax.set_ylim(5*1e-6,1)
                    ax.set_xlim(-1.5,18)
                    ax.set_yscale('log')

            # ax.get_xaxis().set_major_formatter(
                            # matplotlib.ticker.FuncFormatter(lambda x, p: "{0:0.0f}\n[{1:0.1f}]".format(x, x/tx_time)))
            handles, labels = ax.get_legend_handles_labels()
            # sort both labels and handles by labels
            labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
            ax.legend(handles, labels, title='$T_\mathrm{exp}^\mathrm{TRAIN}$', loc= 'lower left')
            ax.axhline(3.8*1e-3, color='black', linestyle='--')

            ax.text(12,(3.8*1e-3)-0.002,'FEC limit = $3.8x10^{-3}$',rotation=0,weight='bold')
            ax.legend(handles, labels, title='$T_\mathrm{exp}^\mathrm{TRAIN}$')
            ax.xaxis.set_major_locator(plt.MaxNLocator(10))
            ax.set_ylabel('BER')
            ax.set_xlabel('SNR')
            # ax.set_ylim(10e-5,1)
            plt.grid(True, 'major', 'y', linestyle='--', linewidth=0.5)
            plt.grid(True, 'minor', 'y', linestyle='--', linewidth=0.3)
# %% PLOT TRAINING LOSS AND VAL_LOSS

import numpy as np
import json
import matplotlib.pyplot as plt
import os
import matplotlib
import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'

layers_v = [2,3]

fig, ax = plt.subplots()
for layers in layers_v:
    if __name__ == '__main__':
        tx_time = 18.9*5
        ber_filename = '/media/cristojv/melon/evaluation/analog_gain_5/mean/hyperoccen_num_layers_{}/ber.json'.format(layers)
        if os.path.exists(ber_filename):
            with open(ber_filename) as json_file:
                ber = json.load(json_file)

                train_exps=[]
                train_losses=[]
                train_val_losses=[]
                for training_exp in ber:
                    # if layers==2:

                    #     plt.title("Two levels topology")
                    # if layers==3:
                    #     plt.title("Three levels topology")

                    train_exp = int(training_exp['train_exp'])
                    train_loss = training_exp['train_loss']
                    train_val_loss = training_exp['train_val_loss']
                    
                    if train_loss is not '':
                        train_exps.append(train_exp)
                        train_losses.append(float(train_loss))
                        train_val_losses.append(float(train_val_loss))
                
                ax.plot(train_exps, train_losses, '--o',color='#1f77b4' if layers==2 else 'orange',label="validation loss [{} stages]".format(layers))
                ax.plot(train_exps, train_val_losses, '--x',color='#1f77b4' if layers==2 else 'orange',label="training loss [{} stages]".format(layers))
                # ax.set_yscale('log')

                # ax.get_xaxis().set_major_formatter(
                                # matplotlib.ticker.FuncFormatter(lambda x, p: "{0:0.0f}\n[{1:0.1f}]".format(x, x/tx_time)))
                handles, labels = ax.get_legend_handles_labels()
                # sort both labels and handles by labels
                labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
                ax.legend(handles, labels, loc= 'upper left')
                ax.xaxis.set_major_locator(plt.MaxNLocator(10))
                ax.set_ylabel('Mean squared error')
                ax.set_xlabel('Exposure time, $t_\mathrm{exp}$ ($\mu s$)')
                ax.set_ylim(0, 0.035)
                plt.grid(True, 'major', 'y', linestyle='--', linewidth=0.5)
                plt.grid(True, 'minor', 'y', linestyle='--', linewidth=0.3)
