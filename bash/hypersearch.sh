source ../.env/bin/activate

python ../hyper_search_sc.py\
 -i '/media/cjurado/synthetic/data_shape-256x16_tclk-100_ts-18900_texp-236000_tchip-94500.npz'\
 -o '/media/cjurado/tuner/chip_pixel_samples_5'\
 -n 'hyperoccen_num_layers_3'\
 -l 3\
 -s 35000\
 -t '../tmp/secrets.json'
python ./telecomms/telebot.py -s "../tmp/secrets.json" -m "[FINISHED] 'search.py'."\