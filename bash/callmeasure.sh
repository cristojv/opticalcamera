source ../.env/bin/activate
exposures=(236 255 406 463 690)
voltages=(18)
voltages_label=(180)
samples=3125

for (( i=0; i<=${#voltages[@]}-1; i++))
do
    for (( n=0; n<=${#exposures[@]}-1; n++))
    do
        python ../callmeasure.py \
            -set \
            -o /media/pi/cristojv/camera/analog_gain_5/chip_pixel_samples_5/$(printf "%04d" ${exposures[n]})us/$(printf "%03d" $((${voltages_label[i]}))) \
            -b '' \
            -nb 32 \
            -ss ${exposures[n]} \
            -v ${voltages[i]} \
            -n ${samples} \
            --zipdata \
            -nr 5 \
	        -ag '5.0' \
            -ts '../tmp/secrets.json'
    done
done