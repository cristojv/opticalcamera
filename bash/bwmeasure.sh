source ../.env/bin/activate
exposures=(85 104 123 142 161 180 198 217 236 255 274 293 312 331 350 \
           369 387 406 425 444 463 482 501 520 539 558 577 595 614 633 652 \
           671 690)
exposures=(85 104 123 142 161 198 236 274 312 350 \
           387 425 444 482 520 558 595 633 \
           671 690)
voltages=(17 16 15 14 13 12 11 10 9 8 7)
samples=1
for (( i=0; i<=${#voltages[@]}-1; i++))
do
    for (( n=0; n<=${#exposures[@]}-1; n++))
    do  
        echo 'idx' ${i}
        echo 'ndx' ${n}
        echo 'exposure' ${exposures[n]}
        echo 'voltage' ${voltages[i]}
        echo $(printf "%04d" ${exposures[0]})_$(printf "%03d" $((${voltages[i]} * 10)))

        python ../callmeasure.py \
            -set -o '/media/pi/cristojv/snr_fft' \
            -b $(printf "%04d" ${exposures[n]})us_$(printf "%03d" $((${voltages[i]}*10)))volt \
            -ss ${exposures[n]} -v ${voltages[i]} -n ${samples} -nb 100
        
        echo "Finished ${exposures[n]}"
    done
done