source ../.env/bin/activate

python ../eval_occen.py \
 -tf ${OCCMEDIA}/tuner \
 -n hyperoccen_num_layers_2 \
 -sf ${OCCMEDIA}/synthetic \
 -cf ${OCCMEDIA}/camera/analog_gain_5 \
 -mf ${OCCMEDIA}/models \
 -of ${OCCMEDIA}/evaluation/analog_gain_5 \
 -te 633 \
 -lv 180 \
 -le 633 652 690 614 595 577 558 \
 -cs 5 \
 -bp 42 \
 -db 0 \
 -t ../tmp/secrets.json
python ../telecomms/telebot.py -s "../tmp/secrets.json" -m "[FINISHED] eval_occen.py."