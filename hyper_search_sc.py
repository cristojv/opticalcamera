import numpy as np
import argparse
import time

from hyper_search_fn import search_hyperparams
from telecomms.telebot import send_message

if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfilename", type=str, required=True,
                    help="Input filename containing synthetic images")
    ap.add_argument("-o", "--outputfolder", type=str, required=True,
                    help="Output folder for saving tuner projects")
    ap.add_argument("-n", "--projectname", type=str, required=True,
                    help="Project name for saving tuner results")
    ap.add_argument("-l", "--layers", type=int, default=None,
                    required=False,
                    help="(Optional) Number of layers if required")
    ap.add_argument("-s", "--datasetsize", type=int, default=1000,
                    required=False,
                    help="(Optional) Dataset size [Default: 1000]")
    ap.add_argument("-t", "--telebotsecrets", type=str,
                    default='./tmp/secrets.json', required=False,
                    help="Secret's location for Telebot")

    args = vars(ap.parse_args())

    input_filename = str(args["inputfilename"])
    output_folder = str(args["outputfolder"])
    project_name = str(args["projectname"])
    num_layers = args["layers"]
    dataset_size = args["datasetsize"]
    secrets_filename = args["telebotsecrets"]

    print("Input filename: {}".format(input_filename))
    print("Output folder: {}".format(output_folder))
    print("Project name: {}".format(project_name))
    print("Number of layers: {}".format(num_layers))
    print("Size of dataset: {}".format(dataset_size))

    msg = "[STARTED] 'search.py' : project name {}".format(project_name)
    print(msg)
    send_message(message=msg, secrets_filename=secrets_filename)
    start = time.time()
    search_hyperparams(input_filename,
                       output_folder,
                       project_name,
                       num_layers=num_layers,
                       dataset_size=dataset_size)
    elapsed_time = time.time() - start
    msg = "[FINISHED] 'search.py': project name {}."\
          " Elapsed time {} seconds".format(project_name, elapsed_time)
    print(msg)
    send_message(message=msg, secrets_filename=secrets_filename)
