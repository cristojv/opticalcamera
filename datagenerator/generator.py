import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import math

class RSCameraGenerator(tf.keras.utils.Sequence):
    '''
    Generates dynamically every batch for the training.
    Loads each independent sample and add noise to them.
    '''
    def __init__(self,
                 df,
                 dataset_folder,
                 dims = (256,16,1),
                 batch_size=32,
                 shuffle = True,
                 noise_range=[0.1,0.2],
                 scale_factor=0.3):

        self._df = df #Dataframe containing the columns 
                      #'exposure_time', 'sampling_period', 
                      # 'sample_id', 'relative_path'
        self._dataset_folder = dataset_folder

        self._dims = dims
        self._noise_range = noise_range

        self._data_size = len(self._df)
        self._batch_size = batch_size
        self._shuffle = shuffle
        self._indexes = np.arange(self._data_size)
    
    def on_epoch_end(self):
        # After one epoch end, shuffle data.
        self._indexes = np.arange(self._data_size)
        if self._shuffle == True:
            np.random.shuffle(self._indexes)

    def __len__(self):
        return int(math.floor(self._data_size / self._batch_size))

    def __getitem__(self, index):
        # Get item returns the batch of data for a given index
        batch_idxs =\
        self._indexes[index * self._batch_size:(index + 1) * self._batch_size]
        return self.__getdata(batch_idxs)

    def __getdata(self, batch_idxs):

        x_data = np.empty((self._batch_size, *self._dims))
        y_data = np.empty((self._batch_size, *self._dims))

        for idx, batch_idx in enumerate(batch_idxs):
            sample = np.load('{}/{}'\
                .format(self._dataset_folder,
                        self._df['relative_path'].iloc[batch_idx]))

            # x_data[idx, ] = np.expand_dims(sample[0], axis=2)
            # y_data[idx, ] = np.expand_dims(sample[1], axis=2)
            x_data[idx, ] = np.expand_dims(sample[0], axis=2)
            y_data[idx, ] = np.expand_dims(sample[1], axis=2)
        
        # Add Batch random noise
        # Create uniform noise_factor matrix from noise range
        noise_factor_m =\
             np.random.uniform(*self._noise_range,size=x_data.shape[0])

        # Expand in the other dimensions
        noise_factor_m = np.expand_dims(noise_factor_m, axis=1)
        noise_factor_m =\
             np.repeat(noise_factor_m, x_data.shape[1], axis=1)
        noise_factor_m = np.expand_dims(noise_factor_m, axis=2)
        noise_factor_m =\
             np.repeat(noise_factor_m, x_data.shape[2], axis=2)
        noise_factor_m = np.expand_dims(noise_factor_m, axis=3)
        noise_factor_m =\
             np.repeat(noise_factor_m, x_data.shape[3], axis=3)
        
        x_data =\
             x_data + noise_factor_m * tf.random.normal(shape=x_data.shape)
        x_data =\
             tf.clip_by_value(x_data, clip_value_min=0., clip_value_max=1.)
        x_data =\
             tf.image.per_image_standardization(x_data)

        return x_data, y_data
        