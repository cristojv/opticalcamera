import numpy as np
import matplotlib.pyplot as plt

from dsp.gen import gen_dataset

if __name__ == '__main__':

    training_exp = 444
    chip_samples = 5
    ts_ticks=189
    
    sig_shape = (256, 16)
    tclk = 100e-9
    tchip_ticks = ts_ticks * chip_samples
    texp_ticks = training_exp*10
    tchip = tchip_ticks * tclk

    x_data, y_data = gen_dataset(output=None,

                                  samples=1,
                                  sig_shape=(100,16),
                                  tclk=tclk,
                                  ts_ticks=ts_ticks,
                                  texp_ticks=texp_ticks,
                                  tchip_ticks=tchip_ticks,
                                  debug=False)

    y_sample = y_data[0,:,:]
    plt.figure(figsize=(5,1))
    plt.plot(y_sample[:,0])
    plt.ylim(-0.1,1.1)
    plt.show()

    x_sample = x_data[0,:,:]
    plt.figure(figsize=(5,1))
    plt.plot(x_sample[:,0])
    plt.ylim(-0.1,1.1)
    plt.show()

    plt.figure()
    plt.imshow(x_sample[:,:])
    plt.show()

    plt.figure()
    plt.imshow(y_sample[:,:])
    plt.show()

    # Normalize
    y_sample = (y_sample - np.min(y_sample)) / (np.max(y_sample) - np.min(y_sample))
    plt.figure(figsize=(5,1))
    plt.plot(y_sample[:,0])
    plt.ylim(-0.1,1.1)
    plt.show()
    
    x_sample = (x_sample - np.min(x_sample)) / (np.max(x_sample) - np.min(x_sample))
    plt.figure(figsize=(5,1))
    plt.plot(x_sample[:,0])
    plt.ylim(-0.1,1.1)
    plt.show()

    # Factor Multiplication
    scale_factor = 0.5
    x_sample = x_sample * scale_factor + (1-scale_factor)/2
    print(np.max(x_sample))
    print(np.min(x_sample))
    plt.figure(figsize=(5,1))
    plt.plot(x_sample[:,0])
    plt.ylim(-0.1,1.1)
    plt.show()

    # Noise addition
    noise_factor=0.07
    x_sample = x_sample + np.random.normal(loc=0,scale=noise_factor,size=x_sample.shape)
    print(np.max(x_sample))
    print(np.min(x_sample))
    plt.figure(figsize=(5,1))
    plt.plot(x_sample[:,0])
    plt.ylim(-0.1,1.1)
    plt.show()

    plt.figure()
    plt.imshow(x_sample[:,:])
    plt.show()

    # Standarization
    x_sample = (x_sample - np.mean(x_sample)) / (np.std(x_sample))
    print(np.max(x_sample))
    print(np.min(x_sample))

    plt.figure()
    plt.imshow(x_sample[:,:])
    plt.show()
