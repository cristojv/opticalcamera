import numpy as np
import pandas as pd
import sys
import os.path
import time
import argparse

# TENSORFLOW
import tensorflow as tf
from tensorflow.keras import callbacks
from kerastuner.tuners import Hyperband

# CUSTOM MODULES
from hyper_search_fn import build_model
from datagenerator.generator import RSCameraGenerator
from acquire import acquire_ber, process_results
# from telecomms.telebot import send_message

# from tensorflow.compat.v1 import ConfigProto
# from tensorflow.compat.v1 import InteractiveSession

# config = ConfigProto()
# config.gpu_options.allow_growth = True
# session = InteractiveSession(config=config)

if __name__ == '__main__':
    
    """python eval_occ_cae_model.py \
        --tunerfolder ${OCCMEDIA}/tuner \
        --synthethicfolder ${OCCMEDIA}/synthetic/dataset \
        --camerafolder ${OCCMEDIA}/camera/analog_gain_5 \
        --modelfolder ${OCCMEDIA}/models \
        --resultsfolder ${OCCMEDIA}/evaluation/analog_gain_5 \
        --projectname hyperoccen_num_layers_3 \
        --datasetsize 35500
        --trainingexposuretimes 161 293 \
        -lv 180 \
        -le 633 652 690 614 595 577 558 \
        -cs 5 \
        -bp 42 \
        --debublevel 0 \
        -t ../tmp/secrets.json"""

    ap = argparse.ArgumentParser()
    # FOLDERS
    ap.add_argument("-tf", "--tunerfolder", type=str, required=False,
                    help="Top level folder that contains the tuner results")
    ap.add_argument("-sf", "--syntheticfolder",
                    required=True, type=str,
                    help="Top level folder for the synthetic dataset files")
    ap.add_argument("-cf", "--camerafolder",
                    required=True, type=str,
                    help="Top level folder for the camera dataset files")
    ap.add_argument("-mf", "--modelfolder",
                    required=True, type=str,
                    help="Top level folder for saving the trained models")
    ap.add_argument("-rf", "--resultsfolder", type=str, required=False,
                    help="Top level folder for saving the training results")

    # PROJECT PARAMETERS
    ap.add_argument("-pn", "--projectname", type=str, required=False,
                    help="Project name for uploading tuner models")
    ap.add_argument("-ds", "--datasetsize", type=int, required=False,
                    default=35500,
                    help="Dataset size [default: 35500]")
    
    ap.add_argument("-te", "--trainingexposuretimes", type=float, nargs='+',
                    required=True, help="List of the selected exposure times "\
                                        "used for training the network.")
    ap.add_argument("-ts", "--trainingsymboltimes", type=float, nargs='+',
                    required=False, help="List of the selected symbol times"\
                                        "s used for training the network.",
                    default=[94.5])
    
    ap.add_argument("-lv", "--listvoltage", type=int, nargs='+', required=True,
                    help="List of voltages in (10*Volts). Remember to substract 180."\
                        "Example: 170 are 17.0 Volts. Total Source voltate = "\
                        "35.0 Volts.")
    
    ap.add_argument("-le", "--listexposure", type=int, nargs='+', required=True,
                    help="List of exposures for evaluation, separated by spaces")

    ap.add_argument("-od", "--overwritedataset", action="store_true",
                    required=False, default=False,
                    help="Overwrite sythetic dataset")
    ap.add_argument("-ot", "--overwritetraining", action="store_true",
                    required=False, default=False,
                    help="Overwrite saved training model")
    ap.add_argument("-or", "--overwriteresults", action="store_true",
                    required=False, default=False,
                    help="Overwrite results")

    ap.add_argument("-cs", "--chippixelsamples", required=True, type=int,
                    help="Chips pixel samples")
    
    ap.add_argument("-bp", "--bitsperpacket", required=True, type=int,
                    help="Number of bits per packet")
    
    ap.add_argument("-db", "--debuglevel", required=False, type=int,
                    help="Debug level  [0 - No debug; 1 - Debug without "\
                         "stopping; 2 - Debub with plots")

    ap.add_argument("-t", "--telebotsecrets", type=str,
                    default='./tmp/secrets.json', required=False,
                    help="Secret's location for Telebot")

    args = vars(ap.parse_args())

    TUNER_FOLDER = args["tunerfolder"]
    SYNTHETHIC_FOLDER = args["syntheticfolder"]
    CAMERA_FOLDER = args["camerafolder"]
    MODEL_FOLDER = args["modelfolder"]
    RESULTS_FOLDER = args["resultsfolder"]

    DATASET_SIZE = args["datasetsize"]
    PROJECT_NAME = args["projectname"]

    SELECTED_EXPOSURE_TIMES = args["trainingexposuretimes"]
    SELECTED_SYMBOL_TIMES = args["trainingsymboltimes"]

    voltages_for_evaluation = args["listvoltage"]
    exposures_for_evaluation = args["listexposure"]

    secrets_filename = args["telebotsecrets"]

    OVERWRITE_DATASET = args["overwritedataset"]
    OVERWRITE_TRAINING = args["overwritetraining"]
    OVERWRITE_RESULTS = args["overwriteresults"]

    chip_samples = args["chippixelsamples"]
    chip_time = int(18900 * chip_samples)

    bits_per_packet = args["bitsperpacket"]

    debug_level = args["debuglevel"]

    MODEL_FILENAME = "{0}/saved.h5".format(MODEL_FOLDER)
    MODEL_CHECKPOINTS_FOLDER = "{}/checkpoints".format(MODEL_FOLDER)
    
    print("RESULTS FOLDER: {}".format(RESULTS_FOLDER))
    print("MODELS FOLDER: {}".format(MODEL_FOLDER))
    print("CAMERA FOLDER: {}".format(CAMERA_FOLDER))
    print("SYNTHETIC FOLDER: {}".format(SYNTHETHIC_FOLDER))

    loaded = False

    if OVERWRITE_TRAINING:
        print("[LOADING MODEL] WARNING: The trained model "\
              "is going to be overwrited.")

    if not OVERWRITE_TRAINING:
        try:
            model = tf.keras.models.load_model(MODEL_FILENAME)

            print("[LOADING MODEL] OK!: Model loaded from {}"\
                 .format(MODEL_FILENAME))
            loaded = True

        except IOError:

            print("[LOADING MODEL] FAIL!: Best model could not be loaded"\
                  " from tuner project name: {}/{}"\
                  .format(TUNER_FOLDER, PROJECT_NAME))
            loaded = False
            
    if OVERWRITE_TRAINING or not loaded:

        print("[BUILDING MODEL]: A new model is being built.")

        tuner = Hyperband(build_model,
                        objective="val_loss",
                        max_epochs=100,
                        factor=3,
                        hyperband_iterations=5,
                        directory = TUNER_FOLDER,
                        project_name = PROJECT_NAME)

        tuner.reload()
        best_hps = tuner.get_best_hyperparameters(1)[0]

        print("[BUILDING MODEL] Best hyperparameters: {}"\
              .format(best_hps.values))

        model = tuner.hypermodel.build(best_hps)

        df = None
        if not os.path.exists(SYNTHETHIC_FOLDER):
            #Generate dataset
            msg = "[LOADING DATASET] WARNING: Dataset dataframe not found. "\
                  "(EXITING)"
            sys.exit()
        else:
            df = pd.read_csv('{}/train.csv'.format(SYNTHETHIC_FOLDER),
                             index_col=0)

            # selected_sampling_periods = [18.9] TODO: Add sampling periods

            train_df = pd.DataFrame() #Empty dataframe
            for exposure in SELECTED_EXPOSURE_TIMES:
                for symbol_time in SELECTED_SYMBOL_TIMES:
                    train_df_sample = df[(df['exposure_time']==exposure)\
                                        & (df['symbol_time']==symbol_time)]
                    if len(train_df_sample) == 0:

                        # TODO: If the exposure is not found then generate 
                        # the corresponding dataset.

                        print("[LOADING DATASET] WARNING: The selected "\
                            "configuration is not within the dataset:\n"\
                            "exposure_time = {} us\n"\
                            "symbol_time = {} us\n"\
                            "(EXITING).".format(exposure, symbol_time))
                        sys.exit()
                    else:
                        train_df = train_df.append(train_df_sample)
            
            print("[LOADING DATASET] OK!: Dataset loaded.")

            # sig_shape = (256, 16)
            # tclk = 100e-9
            # ts_ticks = 189
            # tchip_ticks = ts_ticks * chip_samples
            # texp_ticks = training_exp*10
            # samples = DATASET_SIZE
            # start = time.time()
            # gen_dataset('{}/data'.format(SYNTHETHIC_FOLDER),
            #             samples=samples,
            #             sig_shape=sig_shape,
            #             tclk=tclk,
            #             ts_ticks=ts_ticks,
            #             texp_ticks=texp_ticks,
            #             tchip_ticks=tchip_ticks,
            #             debug=False)
            # msg = "[DATASET]: Generated dataset for training exposure: {}us. Elapsed time: {}".format(training_exp, time.time() - start)
            # print(msg)
            # send_message(msg, secrets_filename)

            if DATASET_SIZE > len(train_df):
                print("[ORGANIZING DATASET] ERROR: The dataset size is less"\
                      " than the number of samples for the selected configur"\
                      "ation. (EXITING)")
                sys.exit()
            
            train_df = train_df.sample(DATASET_SIZE)

            train_factor = 0.9
            msk = np.random.rand(DATASET_SIZE) < train_factor
            train_df_train = train_df[msk].reset_index(drop=True)
            train_df_val = train_df[~msk].reset_index(drop=True)

            print("[ORGANIZING DATASETS] OK!: Two datasets:\n"\
                  " Training dataset with {} samples"\
                  " Validation dataset with {} samples"\
                      .format(len(train_df_train), len(train_df_val)))

            training_generator = RSCameraGenerator(train_df_train,
                                                   SYNTHETHIC_FOLDER,
                                                   dims=(256,16,1),
                                                   batch_size=32,
                                                   shuffle=True,
                                                   noise_range=[0.005, 0.2],
                                                   scale_factor=0.5)
            val_generator = RSCameraGenerator(train_df_val,
                                                   SYNTHETHIC_FOLDER,
                                                   dims=(256,16,1),
                                                   batch_size=32,
                                                   shuffle=True,
                                                   noise_range=[0.005, 0.2],
                                                   scale_factor=0.5)
            
            msg = "[TRAINING]: Starting training"
            print(msg)
            # send_message(msg, secrets_filename)

            start = time.time()

            model_checkpoint_callback = callbacks.ModelCheckpoint(filepath=\
                '{}'\
                    .format(MODEL_CHECKPOINTS_FOLDER),
                monitor='val_loss',
                verbose=1,
                save_best_only=True,
                mode='min',
                save_weights_only=True,
                save_freq='epoch')

            early_stopping_callback = callbacks.EarlyStopping(monitor='val_loss',
                                               mode='min',
                                               verbose=1,
                                               patience=3)
            cb_list = [early_stopping_callback,
                       model_checkpoint_callback]
            
            history = model.fit(training_generator,
                                validation_data = val_generator,
                                epochs = 100,
                                callbacks=cb_list)

            print("[END TRAINING] Ok!: Elapsed time: {0:0.0f}"\
                .format(time.time() - start))

            model.save(MODEL_FILENAME)

            print("[SAVING MODEL]: Ok!: The model has been saved.")
    
    # # Evaluate model
    # with np.load(synthetic_dataset) as data:
    #     x_data = data['x_dat'].astype('float32')
    #     y_data = data['y_dat'].astype('float32')

    #     train_factor = 0.9
    #     val_factor = 0.1
        
    #     val_generator = RSCameraGenerator(\
    #     x_data[-int(math.floor(DATASET_SIZE * val_factor)):],
    #     y_data[-int(math.floor(DATASET_SIZE * val_factor)):],
    #     DATASET_SIZE * val_factor,
    #     batch_size=32,
    #     shuffle=True,
    #     noise_range=[0.005, 0.2],
    #     scale_factor=0.5)

    #     evaluation_results = model.evaluate(val_generator)
    #     print(evaluation_results)

    # results_folder = '{0}/{1}'.format(RESULTS_FOLDER,PROJECT_NAME)
    
    # evaluation_results_filename = '{0}/{1}/eval_training_exposure_{2:04d}us.csv'.format(RESULTS_FOLDER,
    #                                         PROJECT_NAME,
    #                                         training_exp)
    # print("[SAVING EVALUATION RESULTS]")
    # np.savetxt(evaluation_results_filename,
    #         np.asarray(evaluation_results).astype(np.str),
    #         delimiter=',', fmt='%s')
    # for exposure in exposures_for_evaluation:
    #     for voltage in voltages_for_evaluation:
    #         # CHECK if result summary exists
    #         results_filename = '{0}/{1}/res_training_exposure_{2:04d}us'\
    #                         '_selected_exposure_{3:04d}us_volt_{4:03d}v'\
    #                         '.csv'.format(RESULTS_FOLDER,
    #                                         PROJECT_NAME,
    #                                         training_exp,
    #                                         exposure,
    #                                         int(voltage+180))
    #         summary_filename = '{0}/{1}/sum_training_exposure_{2:04d}us'\
    #                         '_selected_exposure_{3:04d}us_volt_{4:03d}v'\
    #                         '.csv'.format(RESULTS_FOLDER,
    #                                         PROJECT_NAME,
    #                                         training_exp,
    #                                         exposure,
    #                                         int(voltage+180))
    #         if os.path.exists(results_filename)\
    #             and os.path.exists(summary_filename)\
    #             and not OVERWRITE_RESULTS\
    #             and not debug_level>=2:
    #                 print("[WARNING]: Not overwriting results")
    #         else:
    #             try:
    #                 os.makedirs(results_folder)
    #             except OSError as e:
    #                 pass
    #             start = time.time()
    #             msg = "[ACQUIRING]: Training exposure: {}, Selected exposure: {}".format(training_exp, exposure)
    #             print(msg)
    #             # send_message(msg, secrets_filename)
    #             results = acquire_ber(model,
    #                                 '{0}/{1:04d}us/{2:03d}'.format(CAMERA_FOLDER,
    #                                                                 exposure,
    #                                                                 voltage),
    #                                 enable_ai=True,
    #                                 debug_level=debug_level,
    #                                 chip_length=chip_samples)
    #             print("[PROCESING RESULTS]")
    #             ber, berm, packets_expected, packets_missed, packets_received, packets_valid = process_results(results, bits_per_packet=bits_per_packet)
                
    #             # Saving results
    #             print("[SAVING RESULTS]")
    #             np.savetxt(results_filename,
    #                     np.asarray(results).astype(np.str),
    #                     delimiter=',', fmt='%s')

    #             # Saving ber
    #             print("[SAVING SUMMARY]: BER {}".format(ber))
    #             np.savetxt(summary_filename,
    #                     np.asarray([ber, berm, packets_expected, packets_missed, packets_received, packets_valid]),
    #                     delimiter=',', fmt='%s')
    #             msg = "[END-ACQUIRING]: Elapsed time: {0:0.0f} seconds".format(time.time() - start)
    #             print(msg)
    #             # send_message(msg, secrets_filename)
