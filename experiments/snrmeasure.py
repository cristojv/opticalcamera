# Import parent dir where the modules of the instruments are located.
import os
import sys
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

import numpy as np
import time
from pymeasure.adapters import VXI11Adapter
from pymeasure.instruments.agilent import Agilent33500
from instruments.YokogawaGS820 import YokogawaGS820

AVAILABLE_EXPOSURE_TIMES = np.asarray([9,28,47,66,85,104,123,142,161,180,198,217,236,255,274,293,312,331,350,369,387,406,425,444,463,482,501,520,539,558,577,595,614,633,652,671,690])
NUMBER_OF_SAMPLES_FOR_EXPOSURE = len(AVAILABLE_EXPOSURE_TIMES)
EXPOSURE_TIMES = AVAILABLE_EXPOSURE_TIMES[np.linspace(1,len(AVAILABLE_EXPOSURE_TIMES)-1,
                                                      NUMBER_OF_SAMPLES_FOR_EXPOSURE).astype(np.int)]
VOLTAGES = np.arange(17,6.5,-0.5)
HOST = '10.13.30.118'

pws = YokogawaGS820(VXI11Adapter(HOST))
# Initial configuration
pws.source1_voltage=18
pws.source1_enabled='on'
pws.source2_voltage=0
pws.source2_enabled='on'

adapter = VXI11Adapter('10.13.30.17')
wfg = Agilent33500(adapter)
wfg.shape = 'DC'
wfg.voltage = 3.8
wfg.offset = 3.8

for voltage in VOLTAGES:
    print("VOLTAGE: {}".format(voltage))
    # 1) Change voltage
    pws.source2_voltage=voltage
    time.sleep(1)
    for ss in EXPOSURE_TIMES:
        print("EXPOSURE_TIME: {}".format(ss))
        # 1) Take photo with the exposure time
        os.system('ssh idetic.local117 "raspistill -ex off -ss {} -ag 12.0 -drc off -w 1920 -h 1080 -md 1 -o /media/pi/cristojv/snr/{:05d}us_volt_{:03d}_on.jpg -t 10"'.format(ss,ss,int(voltage*10)))

pws.source2_enabled='off'
time.sleep(0.5)    
for ss in EXPOSURE_TIMES:
    os.system('ssh idetic.local117 "raspistill -ex off -ss {} -ag 12.0 -drc off -w 1920 -h 1080 -md 1 -o /media/pi/cristojv/snr/{:05d}us_off.jpg -t 10"'.format(ss,ss))

