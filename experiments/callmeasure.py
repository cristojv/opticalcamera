# Import parent dir where the modules of the instruments are located.
import os
import sys
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

import numpy as np
import os
import subprocess
from telecomms.telebot import send_message
import time
import sys
import argparse
from pymeasure.adapters import VXI11Adapter
from pymeasure.instruments.agilent import Agilent33500

from instruments.waveform import GenRandomSig
from instruments.YokogawaGS820 import YokogawaGS820

def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__

ap = argparse.ArgumentParser()
ap.add_argument("-o", "--outputfolder", type=str,
                required=True, help='Output folder')
ap.add_argument("-b", "--basefilename", type=str,
                required=True, help='Base filename')
ap.add_argument("-ss", "--shutterspeed", type=int,
                required=True, help='Shutter speed')
ap.add_argument("-n", "--nsamples", type=int,
                required=True, help="Number of samples")
ap.add_argument("-v", "--voltage", type=float,
                required=True, help="Source voltage from [7 to 18]")
ap.add_argument("-nb", "--nbits", type=int,
                required=False, default=8, help="Number of random bits")
ap.add_argument("-rs", "--randomseed", type=int,
                required=False, default=31415,
                help="Random seed for generating the binary data")
ap.add_argument("-set", "--settings", action="store_true",
                required=False, default=False,
                help="Console output the selected configuration")
ap.add_argument("-z", "--zipdata", action="store_true",
                required=False, default=False,
                help="Zip binary data with header and trailer sequences")
ap.add_argument("-nr", "--nrows", type=int, required=False, default=15,
                help="(Optional) Number of pixel rows per chip [default=15]")
ap.add_argument("-ag", "--analoggain", type=float, required=False, default=12.0,
                help="Analog gain [from 1.0 to 12.0]")
ap.add_argument("-ts", "--telebotsecrets", type=str, required=False,
                default='./tmp/secrets.json',
                help="Secret0s location for Telebot")

args = vars(ap.parse_args())

output_folder = str(args["outputfolder"])
base_filename = str(args["basefilename"])
exposure_time = int(args["shutterspeed"])
samples = int(args["nsamples"])
voltage = float(args["voltage"])
n_bits = int(args["nbits"])
random_seed = int(args["randomseed"])
settings = args["settings"]
zip_data = args["zipdata"]
n_rows = args["nrows"]
analog_gain = args["analoggain"]
chip_frequency = 1/(18.904*1e-6 * n_rows)

telebot_secrets = args["telebotsecrets"]

gain_params = '-ag {0} -dg 1.0 -awb off -awbg 1.895,1.551'.format(analog_gain)
default_params = '-ex off ' + gain_params + ' -w 1920 -h 1080 -md 1 -t 30'
sampling_factor = 100

if settings:
    print("The selected output folder: {}".format(output_folder))
    print("The selected shutter speed: {}".format(exposure_time))
    print("The selected number of samples: {}".format(samples))
    print("The selected voltage: {}".format(voltage))
    print("The selected number of random bits: {}".format(n_bits))
    print("The selected config for zip data: {}".format(zip_data))
    print("The selected chip frequency: 1/({}x18.904us)x{} = {}".format(n_rows,\
            round(chip_frequency*sampling_factor),
            sampling_factor))
    print("The selected analog gain: {}".format(analog_gain))

msg = "START(callmeasure) : Generating {0} ({1} bits | Tx: {2} us | Chips: {3} pixels)"\
      "for exposure time {4} with voltage {5}"\
      ".".format(samples,n_bits,18.904*n_rows, n_rows, exposure_time, voltage)

send_message(msg, telebot_secrets)

# Random signal generator
grs = GenRandomSig(random_seed)
# Arbitrary waveform generator
wfg = Agilent33500(VXI11Adapter('10.13.30.17'))
wfg.shape = 'ARB'
wfg.arb_srate = round(chip_frequency*sampling_factor)
wfg.amplitude = 3.89
# Power suply
pws = YokogawaGS820(VXI11Adapter('10.13.30.118'))
pws.source1_voltage=18
pws.source1_enabled='on'
pws.source2_voltage=7
pws.source2_enabled='on'
pws.source2_voltage=voltage
time.sleep(0.5)

start = time.time()

os.system('ssh idetic.local117 '\
            '"mkdir -p {0}"'.format(output_folder))

for idx in range(samples):

    if (idx%1==0):
        print("COMPLETE: {}%".format(idx/samples*100))
    
    # Generate random signal
    sig = grs.gen_random_sig(length_bits=n_bits,
                             factor=sampling_factor,
                             zip=zip_data)
    sig = (sig-1)*-1
    # Send signal to Waveform generator
    wfg.data_volatile_clear()
    blockPrint()
    wfg.data_arb('test',
                sig*20000,
                data_format='DAC')
    wfg.arb_file = 'test'
    enablePrint()
    wfg.amplitude = 3.89
    time.sleep(0.001)
    
    max_retries=5
    retries=0
    while retries < max_retries:
        try:
            subprocess.check_output('ssh idetic.local117 '\
                    '"raspistill {0} -ss {1} '\
                    '-o {2}/{3}_{4:05d}.jpg"'.format(default_params,
                                                exposure_time,
                                                output_folder,
                                                base_filename,
                                                idx),
                    shell=True,timeout=5)
        except subprocess.TimeoutExpired:
            msg = "TIMEOUT(callmeasure) : Exposure: {}, Index: {}".format(exposure_time, idx)
            print(msg)
            send_message(msg, telebot_secrets)
            sleep_time = 60
            time.sleep(sleep_time)

            if retries == (max_retries-1):
                msg = "ERROR(callmeasure) : Restart script"
            else:
                msg = "RETRYING(callmeasure) : Wait {} seconds".format(sleep_time)
                print(msg)
                send_message(msg, telebot_secrets)
            continue
        else:
            break
        finally:
            retries = retries + 1 

# Save random generated bits for comparison
np.savetxt('random_data.csv', grs._saved_values, delimiter=',')
os.system('scp ./random_data.csv '\
          'idetic.local117:{0}/random_data.csv'.format(output_folder))
# Delete local copy
os.system('rm -rf ./random_data.csv')
# Print elapsed time
elapsed_time = time.time() - start
msg = "FINISH(callmeasure) : Exp: {}, Volt: {}, Chips: {}, Elapsed time: {}".format(exposure_time,
                                                                                    voltage,
                                                                                    n_rows,
                                                                                    elapsed_time)
print(msg)
send_message(msg, telebot_secrets)