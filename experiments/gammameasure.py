# Import parent dir where the modules of the instruments are located.
import os
import sys
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

import numpy as np
import time
from pymeasure.adapters import VXI11Adapter
from pymeasure.instruments.agilent import Agilent33500
from instruments.YokogawaGS820 import YokogawaGS820

EXPOSURE_TIME = 85
currents = np.arange(120,0-2,-2)/1000
gain_params = '-ag 12.0 -dg 1.0 -awb off -awbg 1.895,1.551'

if __name__ == '__main__':
    pws = YokogawaGS820(VXI11Adapter('10.13.30.118'))
    pws.source1_mode='current'
    pws.source1_current=20
    pws.source1_enabled='on'

    for current in currents:
        print("Current: {}".format(current))
        pws.source1_current=current
        time.sleep(1)
        print("EXPOSURE_TIME: {}".format(EXPOSURE_TIME))
        os.system('ssh idetic.local117 "mkdir -p /media/pi/cristojv/gamma/current/"')
        os.system('ssh idetic.local117 "raspistill --raw -ex off -ss {0} {1} -w 1920 -h 1080 -o /media/pi/cristojv/gamma/current/{0:05d}us_volt_{2:03d}_gamma.jpg -t 10"'.format(EXPOSURE_TIME,gain_params,int(current*1000)))