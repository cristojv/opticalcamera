import numpy as np
import matplotlib.pyplot as plt
import argparse
import time
import os.path
import math

import tensorflow as tf
from tensorflow.keras import callbacks
from kerastuner.tuners import Hyperband

from hyper_search_fn import build_model
from datagenerator.generator import RSCameraGenerator

from rollingshutter.gen import gen_dataset

if __name__ == '__main__':
    ap = argparse.ArgumentParser()

    ap.add_argument("-o", "--outputfolder", type=str, required=True,
                    help="Output folder")
    ap.add_argument("-tf", "--tunerfolder", type=str, required=True,
                    help="Input folder that contains tuner results")
    ap.add_argument("-n", "--projectname", type=str, required=True,
                    help="Project name for uploading tuner models")
    ap.add_argument("-sd", "--syntheticdataset",
                    required=True, type=str,
                    help="Synthetic images dataset")
    ap.add_argument("-s", "--datasetsize", type=int, required=False,
                    default=35500,
                    help="(Optional) Dataset size [default: 1000]")
    ap.add_argument("-te", "--trainingexposure", type=int, required=True,
                    help="Training exposure in us")
    ap.add_argument("-v", "--voltage", type=int, required=False,
                    help="Voltage in (10*Volts). Remember to substract 180."\
                        "Example: 170 are 17.0 Volts. Total Source voltate = "\
                        "35.0 Volts.")
    ap.add_argument("-cs", "--chippixelsamples", required=True, type=int,
                    help="Chips pixel samples")
    args = vars(ap.parse_args())

    output_folder = args["outputfolder"]
    tuner_folder = args["tunerfolder"]
    project_name = args["projectname"]
    synthetic_dataset_folder = args["syntheticdataset"]
    dataset_size = args["datasetsize"]
    training_exp = args["trainingexposure"]
    voltage = args["voltage"]
    chip_samples = args["chippixelsamples"]
    chip_time = int(18900 * chip_samples)
    
    output_folder = '{0}/chip_pixel_samples_{1}'.format(output_folder,
                                                       chip_samples)

    model_folder = '{0}/{1}/training_exp_{2:04d}us'.format(output_folder,
                                                           project_name,
                                                           training_exp)
    synthetic_dataset = '{0}/data_shape-256x16_tclk-100_ts-18900_texp-{1}_tchip-{2}.npz'.format(synthetic_dataset_folder,
                                                                                                int(training_exp*1000),
                                                                                                chip_time)
    # Loading model
    try:
        model = tf.keras.models.load_model(model_folder)
        print("[LOADING MODEL] OK!: Model loaded from {}".format(model_folder))
    except IOError:
        print("[LOADING MODEL] FAIL!: Loading Best model from tuner project name: {}".format(project_name))
        tuner = Hyperband(build_model,
                        objective="val_loss",
                        max_epochs=100,
                        factor=3,
                        hyperband_iterations=5,
                        directory = tuner_folder,
                        project_name = project_name)

        tuner.reload()
        best_hps = tuner.get_best_hyperparameters(1)[0]
        print("Best hyperparameters: {}".format(best_hps.values))
        model = tuner.hypermodel.build(best_hps)
        
        if not os.path.exists(synthetic_dataset):
            #Generate dataset
            msg = "[DATASET]: Generating dataset for training exposure: {}".format(training_exp)
            print(msg)
            
            sig_shape = (256, 16)
            tclk = 10e-9
            sampling_ticks = round(18.9e-6/tclk)
            txsymbol_ticks = sampling_ticks * chip_samples
            exposure_ticks = round((training_exp*1e-6)/tclk)
            samples = dataset_size
            start = time.time()
            gen_dataset(output='{}/data'.format(synthetic_dataset_folder),
                        samples=samples,
                        shape=sig_shape,
                        tclk=tclk,
                        txsymbol_ticks=txsymbol_ticks,
                        sampling_ticks=sampling_ticks,
                        exposure_ticks=exposure_ticks,
                        debug=False)
            msg = "[DATASET]: Generated dataset for training exposure: {}us. Elapsed time: {}".format(training_exp, time.time() - start)
            print(msg)
        
        with np.load(synthetic_dataset) as data:
            x_data = data['x_dat'].astype('float32')
            y_data = data['y_dat'].astype('float32')

            train_factor = 0.9
            val_factor = 0.1
            training_generator = RSCameraGenerator(\
                    x_data[:int(math.floor(dataset_size * train_factor))],
                    y_data[:int(math.floor(dataset_size * train_factor))],
                    dataset_size * train_factor,
                    batch_size=32,
                    shuffle=True,
                    noise_range=[0.05, 0.3])
            val_generator = RSCameraGenerator(\
                x_data[-int(math.floor(dataset_size * val_factor)):],
                y_data[-int(math.floor(dataset_size * val_factor)):],
                dataset_size * val_factor,
                batch_size=32,
                shuffle=True,
                noise_range=[0.05, 0.3])

            cb_list = [callbacks.EarlyStopping(monitor='val_loss',
                                                mode='min',
                                                verbose=1,
                                                patience=3)]
            
            msg = "[TRAINING]: Training exposure: {}".format(training_exp)
            print(msg)
            start = time.time()
            history = model.fit(training_generator,
                                validation_data = val_generator,
                                epochs = 100,
                                callbacks=cb_list)
            model.save(model_folder)
            msg = "[END-TRAINING]: Elapsed time: {0:0.0f}".format(time.time() - start)
            print(msg)
    
    # Encode and decode images
    sig_shape = (256, 16)
    tclk = 10e-9
    sampling_ticks = round(18.9e-6/tclk)
    txsymbol_ticks = sampling_ticks * chip_samples
    exposure_ticks = round((training_exp*1e-6)/tclk)
    x_data, y_data = gen_dataset(output=None,
                                 samples=5,
                                 shape=sig_shape,
                                 tclk=tclk,
                                 txsymbol_ticks=txsymbol_ticks,
                                 sampling_ticks=sampling_ticks,
                                 exposure_ticks=exposure_ticks,
                                 debug=False)
    noise_factor = 0.01
    x_data = x_data + noise_factor * tf.random.normal(shape=x_data.shape)
    x_data = tf.clip_by_value(x_data, clip_value_min=0., clip_value_max=1.)
    x_data = tf.image.per_image_standardization(x_data)
    x_data = x_data[..., tf.newaxis]
    y_data = y_data[..., tf.newaxis]

    encoded_imgs = model.encoder(x_data).numpy()
    decoded_imgs = model.decoder(encoded_imgs).numpy()
    
    for idx, decoded_img in enumerate(decoded_imgs):
        fig, ax = plt.subplots(1,3)
        ax[0].imshow(x_data[idx])
        ax[1].imshow(decoded_img)
        ax[2].imshow(y_data[idx])
    
    plt.show()
    print(encoded_imgs.shape)
    print(encoded_imgs.shape[-1])
    print(encoded_imgs[0,:,:,0].shape)