import numpy as np
import matplotlib.pyplot as plt
import cv2
import tensorflow as tf
import time
import argparse
from hyper_search_fn import reload_best_models, build_model
from scipy.signal import find_peaks
from enum import Enum
from instruments.waveform import manchester_encoding, upsample

class Msg(Enum):
    VALID = 1
    NOT_VALID = 0
    NOT_DETECTED = 2
    PARTIAL = 3

def generate_header_template(chip_length=15, header_chips=5):
    template = np.ones(header_chips)
    template = np.insert(template, 0, 0)
    template = manchester_encoding(template)
    template = (template-1)*-1
    template = upsample(template, chip_length)
    template = np.expand_dims(template, axis=1)
    template = template.astype(np.float32)
    # h, w = template.shape
    return template

def generate_window_template(chip_length=15):
    window = np.ones(1)
    window = manchester_encoding(window)
    window = upsample(window, chip_length)
    window = (window * 2) - 1
    window = (window-1)*-1
    window = (window - np.mean(window))/np.std(window)
    return window

def acquire_ber(model, input_folder,
                enable_ai=True,
                debug_level=0,
                chip_length=15,
                averageColumns=False,
                selectedColumn=960):
    results = []
    chip_length = chip_length
    header_chips = 5
    thr_level = 0.6
    width = 16
    template = generate_header_template(chip_length, header_chips)
    window = generate_window_template(chip_length)
    
    # Load validation images
    reader = cv2.VideoCapture('{}/_%05d.jpg'.format(input_folder))
    # Load expected bits
    expected_bits = np.genfromtxt('{}/random_data.csv'.format(input_folder),
                        delimiter=',')
    expected_bits = expected_bits.astype(np.int)
    number_of_bits = expected_bits.shape[1]
    print(number_of_bits)
    
    # Start
    idx = -1
    s = True
    while s:
        idx = idx + 1
        s, img = reader.read()
        if not s:
            print("The experiment has finished!")
            break

        if (idx%10 == 0):
            print("COMPLETED: {}".format(idx))
        
        gamma = 2.4

        # Average the row pixel values for all the columns of the image.
        if averageColumns:
            img = (img[:,:,1]/255.0)**(1/gamma)
            img = np.mean(img, axis=1)
            img = np.repeat(np.expand_dims(img, axis=1),width,axis=1)

        # Do NOT average the row pixel values for all the image columns.
        else:
            start=selectedColumn
            img = (img[:, start:start+width, 1]/255.0)**(1/gamma)
        
        if enable_ai:
            # Get all the images sections
            images = []
            number_of_segments = 6
            valid_pixels = 1080/number_of_segments
            overlapping = 256 - valid_pixels
            overlapping_top = int(overlapping//2)
            overlapping_bottom = overlapping_top
            segments = []
            if debug_level >=3:
                print("Number of segments: {}".format(number_of_segments))
                print("Valid segment length: {}".format(valid_pixels))
                print("Total overlapping pixels: {}".format(overlapping))
                print("Top overlapping pixels: {}".format(overlapping_top))
                print("Bottom overlapping pixels: {}".format(overlapping_bottom))

                idx0 = (0,256)
                idx1 = (216-overlapping_top)
                idx2 = (216*2-overlapping_top)
            if debug_level >= 3:
                img_rectangles = img.copy()
            for i in range(number_of_segments):
                if (i == 0):
                    top=0
                elif (i == number_of_segments-1):
                    top=1080-256
                else:
                    top = valid_pixels*i - overlapping_top
                
                top = int(top)
                bot = top+256
                # print("TOP: {}, BOT: {}".format(top, bot))
                segment = slice(top, bot)
                segments.append(segment)
                img_s = img[segment]
                images.append(img_s)

                if debug_level >= 3:
                    img_rectangles=cv2.rectangle(img_rectangles, (0,top), (int(width),bot), (255,255,255), 3)
            
            if debug_level >= 3:
                plt.imshow(img_rectangles)
                plt.show()
            images = np.asarray(images)
            images = images[..., tf.newaxis]

            images = tf.image.per_image_standardization(images)
            if debug_level == 3:
                plt.plot(images[0,:,0])
                plt.show()
            real_encoded_imgs = model.encoder(images).numpy()
            real_decoded_imgs = model.decoder(real_encoded_imgs).numpy()
            
            fusion = np.ones((256,16))
            fusion_top = fusion.copy()
            fusion_bottom = fusion.copy()
            step = int(overlapping_top)
            fusion_top[0:step,:] =\
                np.repeat(
                    np.expand_dims(
                        np.arange(1,step+1)/step,
                        axis=1),
                    16,axis=1)
            fusion_bottom[-step:,:] =\
                np.repeat(
                    np.expand_dims(
                        np.arange(step-1,-1,-1)/step,
                        axis=1),
                    16,axis=1)

            fusion_top[fusion_top<1]=0
            fusion_bottom[fusion_bottom<1]=0
            img_g = np.zeros((1080,16))
            img_g_0 = np.squeeze(real_decoded_imgs[0])
            img_g_0 = img_g_0[:256-overlapping_bottom]
            fusion_0 = fusion_bottom[overlapping_bottom:]

            # if debug_level>=3:
            #     print(img_g_0.shape)
            #     print(fusion_0.shape)
            #     plt.imshow(fusion_top)
            #     plt.show()
            #     plt.imshow(fusion_bottom)
            #     plt.show()
            #     plt.imshow(fusion_top*fusion_bottom)
            #     plt.show()

            img_g[:256-overlapping_top,:] = img_g_0*fusion_0
            
            if debug_level>=3:
                plt.figure()
                plt.imshow(img_g)

            for i in range(1,number_of_segments-1):
                top = int(i*valid_pixels-overlapping_top)
                img_g[top:top+256,:] =\
                    img_g[top:top+256,:] +\
                    np.squeeze(real_decoded_imgs[i])* fusion_top * fusion_bottom
                
                # if debug_level >= 3:
                #     plt.figure()
                #     plt.imshow(img_g)

            img_g_5=np.squeeze(real_decoded_imgs[-1])
            img_g_5 = img_g_5[overlapping_top:]
            fusion_5 = fusion_top[:256-overlapping_top]
            img_g[-256+overlapping_top:,:] = img_g[-256+overlapping_top:,:] + img_g_5*fusion_5
            img_g = np.clip(img_g,0,1)

            if debug_level >= 3:
                plt.figure()
                plt.imshow(img_g_5)
                plt.figure()
                plt.imshow(real_decoded_imgs[-1])
                plt.figure()
                plt.imshow(img_g_5*fusion_5)
                plt.imshow(img_g)
                plt.show()
            img_m = np.expand_dims(np.mean(img_g, axis=1),axis=1)
        else:
            img_m = np.mean(img, axis=1)

        img_m = img_m.astype(np.float32)
        img_m = (img_m - np.mean(img_m))/np.std(img_m)
        
        res = cv2.matchTemplate(img_m, template, cv2.TM_CCOEFF_NORMED)
        peaks, _ = find_peaks(res.flatten(), height=thr_level)
        
        if (len(peaks)<=0):
            print("ERROR: Packet not detected with thr: {}".format(thr_level))
            if debug_level>=2:
                plt.figure()
                plt.plot(res)
                plt.title("Correlation result")
                plt.show()
                if enable_ai:
                    plt.figure()
                    plt.plot(img_g)
                    plt.title("Image generated with AI")
                    plt.show()
                else:
                    plt.figure()
                    plt.plot(img_m)
                    plt.title("Original image")
                    plt.show()
            results.append([str(Msg.NOT_DETECTED.value),0,0,0])
            continue

        peaks = np.append(np.expand_dims(res.flatten()[peaks],axis=1),
                        np.expand_dims(peaks, axis=1),axis=1)
        peaks = peaks[peaks[:,0].argsort(kind='mergesort')][::-1]
        
        found = False
        peak = None
        for val in peaks:
            peak = int(val[1])
            if peak+template.shape[0]+window.shape[0]*(number_of_bits)<=img_m.shape[0]:
                found = True
                break
            
        if not found:
            print("ERROR: Packet partially recovered.")
            results.append([str(Msg.PARTIAL.value),0,0,0])
            if debug_level>=2:
                plt.figure()
                plt.plot(res)
                plt.plot(peaks, res[peaks], "x")
                plt.show()
        else:
            top = peak + template.shape[0]
            bot = top + window.shape[0]*(number_of_bits)
            bits = []
            step = window.shape[0]
            for i in range(number_of_bits):
                bit_img = img_m[top+step*i:top+step*(i+1)].squeeze()
                bit = np.matmul(window, bit_img.T)/len(window)
                bits.append(1 if bit >= 0 else 0)
            
            res_bit = np.sum(np.equal(bits, expected_bits[idx,:]))

            valid = (res_bit == number_of_bits)
            results.append([str(Msg.VALID.value) if valid else str(Msg.NOT_VALID.value),
                            res_bit,
                            ''.join([str(i) for i in bits]),
                            ''.join([str(i) for i in expected_bits[idx,:]])])

            if not valid and debug_level>=2:
                print("ERROR: Bits not recovered successfully")

                print("Recovered bits: {}".format(''.join([str(x) for x in bits])))
                print("Expected bits:  {}".format(''.join([str(x) for x in expected_bits[idx,:]])))
                print("Successfully recovered: {} bits".format(res_bit))

                if enable_ai:
                    plt.figure()
                    img_debug = img_g.copy()
                    img_debug = np.expand_dims(img_debug, axis = 2)
                    img_debug = np.repeat(img_debug,10, axis=1)
                    www = img_debug.shape[1]
                    img_debug = np.repeat(img_debug,3, axis=2)
                    img_debug[:,:,0]=0
                    img_debug[:,:,2]=0
                    print(img_debug.shape)
                    
                    for i in range(number_of_segments):
                        if (i == 0):
                            top_segment=0
                        elif (i == number_of_segments-1):
                            top_segment=1080-256
                        else:
                            top_segment = valid_pixels*i - overlapping_top
                        
                        top_segment = int(top_segment)
                        bot_segment = top_segment+256
                        # print("TOP: {}, BOT: {}".format(top_segment, bot_segment))

                        img_debug=cv2.rectangle(img_debug, (0,top_segment), (www,bot_segment), (1,0,0), 3)
                    img_debug = cv2.rectangle(img_debug,(0,top-template.shape[0]),(www,bot), color=(0,0,1), thickness=5)
                    plt.imshow(img_debug)
                    plt.title("Image generated with AI")
                    plt.figure()
                    plt.imshow(img_g[top:bot, 0:16])
                    plt.show()
                plt.figure()
                plt.plot(img_m)
                img_o = img.copy()
                plt.plot(img_o[:,0])
                plt.title("Original image")
                plt.show()

    return results

def process_results(results, bits_per_packet=10):
    pber = np.asarray(results.copy())[:,0]
    pber = np.asarray([int(a) for a in pber])
    idx = np.where(pber>=2)
    packets_missed = idx[0].shape[0]
    pber[idx] = 0
    packets_expected = pber.shape[0]
    packets_received = pber.shape[0] - packets_missed
    packets_valid = np.sum(pber)
    pber = (packets_received - packets_valid) / (packets_received)

    ber = np.asarray(results.copy())[:,1]
    ber = np.asarray([int(a) for a in ber])
    bits_valid = np.sum(ber)
    bits_expected = packets_expected * bits_per_packet
    bits_received = packets_received * bits_per_packet
    ber = (bits_expected - bits_valid)/bits_expected
    berm = (bits_received - bits_valid)/bits_received

    print("Number of expected bits: {}".format(bits_expected))
    print("Number of received bits: {}".format(bits_received))
    print("Number of valid bits: {}".format(bits_valid))
    print("Ber: {}".format(ber))
    print("Berm: {}".format(berm))

    return ber, berm, packets_expected, packets_missed, packets_received, packets_valid, 

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfolder", type=str, required=True,
                    help="Input folder with the photos for being scanned")
    ap.add_argument("-ai", "--artificialintelligence", action="store_true",
                    required=False, default=False,
                    help="Enable Artificial Intelligence for decoding")
    ap.add_argument("-tf", "--tunerfolder", type=str, required=True,
                    help="Folder that contains the tuner project")
    ap.add_argument("-tn", "--tunername", type=str, required=True,
                    help="Tuner project name")
    ap.add_argument("-dl", "--debuglevel", type=int, required=False,
                    default=0, help="Set debug level. The program would be"\
                    "interrupted after one error occurs")
    ap.add_argument("-nr", "--nrows", type=int, required=True,
                    default=0, help="Set debug level. The program would be"\
                    "interrupted after one error occurs")
    args = vars(ap.parse_args())
    input_folder = str(args["inputfolder"])
    enable_ai = bool(args["artificialintelligence"])
    tuner_folder = str(args["tunerfolder"])
    tuner_name = str(args["tunername"])
    debug_level = int(args["debuglevel"])
    n_rows = int(args["nrows"])

    if debug_level>=0:
        print("Selected Input folder: {}".format(input_folder))
        print("AI is enabled: {}".format(enable_ai))
        print("Selected Tuner folder: {}".format(tuner_folder))
        print("Selected Tuner project: {}".format(tuner_name))
        print("Debug level: {}".format(debug_level))
        print("Chip length: {}".format(n_rows))

    template = generate_header_template()
    window = generate_window_template()

    if enable_ai:
        models = reload_best_models(1, tuner_folder, tuner_name, build_model)
        model = models[0]

    start = time.time()
    results = acquire_ber(None, input_folder, enable_ai=enable_ai, debug_level=debug_level, chip_length=n_rows)
    print("Elapsed time: {:0.0f} seconds".format(time.time() - start))

    if debug_level == 0:

        ber, packets_valid, packets_received = process_results(results)
        print("BER: {}".format(ber))

        np.savetxt('{}/results.csv'.format(input_folder),
                np.asarray(results).astype(np.str),
                delimiter=',', fmt='%s')
        np.savetxt('{}/summary.csv'.format(input_folder),
                np.asarray([ber, packets_valid, packets_received]),
                delimiter=',', fmt='%s')
