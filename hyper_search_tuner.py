import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow_transform as tft
from tensorflow.keras import Model, layers, regularizers

class HyperConvAutoencoder(Model):
    def __init__(self, hyperparams):
        super(HyperConvAutoencoder, self).__init__()
        
        # ENCODER
        self.encoder = tf.keras.Sequential()
        self.encoder.add(layers.Input(shape=(256, 16, 1)))

        for i in range(hyperparams.Int('num_layers', 2, 4)):
            with hyperparams.conditional_scope('num_layers',
                                               list(range(i+1, 4+1))):
                self.encoder.add(layers.Conv2D(\
                    filters=hyperparams.Int('filters_' + str(i),
                                            min_value=8,
                                            max_value=64,
                                            step=8),
                    kernel_size=(hyperparams.Int('kernel_height_' + str(i),
                                                 min_value=2,
                                                 max_value=5,
                                                 step=1),
                                 hyperparams.Int('kernel_width_' + str(i),
                                                 min_value=2,
                                                 max_value=5,
                                                 step=1)),
                    activation='relu',
                    padding='same',
                    strides=(1,1),
                    kernel_initializer='glorot_uniform',
                    kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)))

                self.encoder.add(layers.MaxPool2D(\
                    pool_size=(hyperparams.Int('pool_height_' + str(i),
                                               min_value=1,
                                               max_value=2),
                               hyperparams.Int('pool_width_' + str(i),
                                               min_value=1,
                                               max_value=2))))
        # DECODER
        self.decoder = tf.keras.Sequential()
        self.decoder.add(layers.Input(\
            shape=np.asarray(self.encoder.output_shape)[1:]))
        
        j = 0
        for i in range(hyperparams.get('num_layers')-1,-1,-1):
            j=i
            print("layer {}".format(i))
            with hyperparams.conditional_scope('num_layers',
                                               list(range(i+1, 4+1))):
                self.decoder.add(layers.Conv2DTranspose(\
                    filters=hyperparams.get('filters_' + str(i)),
                    kernel_size=(hyperparams.get('kernel_height_' + str(i)),
                                 hyperparams.get('kernel_width_' + str(i))),
                    strides=(1,1),
                    activation='relu',
                    padding='same',
                    kernel_initializer='glorot_uniform',
                    kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)))

                self.decoder.add(layers.UpSampling2D(\
                    size=(hyperparams.get('pool_height_' + str(i)),
                          hyperparams.get('pool_width_' + str(i)))))

        self.decoder.add(layers.Conv2D(\
            filters=1,
            kernel_size=(hyperparams.get('kernel_height_' + str(j)),
                         hyperparams.get('kernel_width_' + str(i))),
            activation='sigmoid',
            padding='same',
            kernel_initializer='random_normal',
            kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)))

        print("SUMMARY")
        print("--------------------------------")
        print("ENCODER SUMMARY")
        print(self.encoder.summary())
        print("DECODER SUMMARY")
        print(self.decoder.summary())
    def call(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded