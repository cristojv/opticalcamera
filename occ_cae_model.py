import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

# from tensorflow.compat.v1 import ConfigProto
# from tensorflow.compat.v1 import InteractiveSession

# config = ConfigProto()
# config.gpu_options.allow_growth = True
# session = InteractiveSession(config=config)

from tensorflow.keras import layers, regularizers, losses, Model

import cv2
from datagenerator.generator import RSCameraGenerator

class ConvAutoencoder(Model):
    def __init__(self):
        super(ConvAutoencoder, self).__init__()
        
        # ENCODER
        self.encoder = tf.keras.Sequential()
        self.encoder.add(layers.Input(shape=(256, 16, 1)))
        self.encoder.add(layers.Conv2D(32, (3,3),
                          activation='relu',
                          padding='same', strides=1,
                          kernel_initializer='random_normal'))
        self.encoder.add(layers.MaxPool2D(pool_size=(1,2)))
        self.encoder.add(layers.Conv2D(16, (3,3),
                          activation='relu',
                          padding='same', strides=1,
                          kernel_initializer='random_normal'))
        self.encoder.add(layers.MaxPool2D(pool_size=(2,2)))
        self.encoder.add(layers.Conv2D(8, (3,3),
                          activation='relu',
                          padding='same', strides=1,
                          kernel_initializer='random_normal'))
        self.encoder.add(layers.MaxPool2D(pool_size=(2,1)))

        # DECODER
        self.decoder = tf.keras.Sequential()
        self.decoder.add(layers.Conv2DTranspose(8,
                                                kernel_size=3,
                                                strides=1,
                                                activation='relu',
                                                padding='same',
                                                kernel_initializer='random_normal',
                                                kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),))
        self.decoder.add(layers.UpSampling2D(size=(2,1)))
        self.decoder.add(layers.Conv2DTranspose(16,
                                                kernel_size=3,
                                                strides=1,
                                                activation='relu',
                                                padding='same',
                                                kernel_initializer='random_normal',
                                                kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),))
        self.decoder.add(layers.UpSampling2D(size=(2,2)))
        self.decoder.add(layers.Conv2DTranspose(32,
                                                kernel_size=3,
                                                strides=1,
                                                activation='relu',
                                                padding='same',
                                                kernel_initializer='random_normal',
                                                kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),))
        self.decoder.add(layers.UpSampling2D(size=(1,2)))
        self.decoder.add(layers.Conv2D(1, kernel_size=(3,3), activation='sigmoid', padding='same', kernel_initializer='random_normal',kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)))
    
    def call(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded

def normalize_with_moments(x, axes=[0, 1], epsilon=1e-8):
    mean, variance = tf.nn.moments(x, axes=axes)
    x_normed = (x - mean) / tf.sqrt(variance + epsilon) # epsilon to avoid dividing by zero
    return x_normed

autoencoder = None
model_filepath = './tmp/autoencoder_withuniformnoises'
try:
    autoencoder = tf.keras.models.load_model(model_filepath)
    print("LOADED")
except IOError:
    print("NOT LOADED: Print generating and saving new model")
    autoencoder = ConvAutoencoder()
    autoencoder.compile(optimizer='adam', loss=losses.MeanSquaredError())

input = './dataset/data_shape-256x16_tclk-0_ts-19_texp-444_tchip-284.npz'
with np.load(input) as data:
    x_data = data['x_dat'].astype('float32')
    y_data = data['y_dat'].astype('float32')

    # # Add noise
    # noise_factor = 0.1
    # x_data_noise = x_data + noise_factor * tf.random.normal(shape=x_data.shape)
    # x_data_clipped = tf.clip_by_value(x_data_noise, clip_value_min=0., clip_value_max=1.)
    # # x_data_norm = normalize_with_moments(x_data_clipped)
    # x_data_norm = tf.image.per_image_standardization(x_data_clipped)
    # plt.plot(x_data_noise[0,:,0].numpy())
    # plt.figure()
    # plt.plot(x_data_clipped[0,:,0].numpy())
    # plt.figure()
    # plt.plot(x_data_norm[0,:,0].numpy())
    # plt.show()

    DATASET_SIZE = 40000
    train_factor = 0.9
    val_factor = 0.1

    training_generator = RSCameraGenerator(x_data[:int(DATASET_SIZE * train_factor)],
                                           y_data[:int(DATASET_SIZE * train_factor)],
                                           DATASET_SIZE * train_factor,
                                           batch_size=32,
                                           shuffle=True,
                                           noise_range=[0.1, 0.3])
    val_generator = RSCameraGenerator(x_data[-int(DATASET_SIZE * val_factor):],
                                      y_data[-int(DATASET_SIZE * val_factor):],
                                      DATASET_SIZE * val_factor,
                                      batch_size=32,
                                      shuffle=True,
                                      noise_range=[0.1, 0.3])
    # LOAD Dataset from Memory
    # dataset = tf.data.Dataset.from_tensor_slices((x_data_norm, y_data))
    
    # train_dataset = dataset.take(int(train_factor*DATASET_SIZE))
    # val_dataset = dataset.skip(int(train_factor*DATASET_SIZE))

    # BATCH_SIZE = 32
    # SHUFFLE_BUFFER_SIZE = 100

    # train_dataset = dataset.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
    # val_dataset = val_dataset.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)

    cb_list = [tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                mode='min',
                                                verbose=1,
                                                patience=5),
               tf.keras.callbacks.ModelCheckpoint(filepath=model_filepath,
                                                #   monitor='val_loss',
                                                  verbose=1,
                                                  save_best_only=False,
                                                #   mode='min',
                                                  period=1,
                                                  save_weights_only=False)]
    # autoencoder.fit(training_generator,
    #                 validation_data=val_generator,
    #                 use_multiprocessing=False,
    #                 # workers=6,
    #                 epochs=10,
    #                 callbacks=cb_list
    #                 )

    autoencoder.save(model_filepath)
    # autoencoder.load_weights(model_filepath)
# %%
# Generate 5 random images and plot

import gen

sig_shape = (256, 16)
tclk = 100e-9
ts_ticks = 189
tchip_ticks = 2835
texp_ticks = 4440
tchip = tchip_ticks * tclk
debug = False

x_data, y_data = gen.gen_dataset(None,samples=5,sig_shape=sig_shape,tclk=tclk,ts_ticks=ts_ticks,texp_ticks=texp_ticks,t_chip=tchip,debug=debug)
noise_factor = 0.3
x_data = x_data + noise_factor * tf.random.normal(shape=x_data.shape)
x_data = tf.clip_by_value(x_data, clip_value_min=0., clip_value_max=1.)
x_data = tf.image.per_image_standardization(x_data)
x_data = x_data[..., tf.newaxis]
y_data = y_data[..., tf.newaxis]

encoded_imgs = autoencoder.encoder(x_data).numpy()
decoded_imgs = autoencoder.decoder(encoded_imgs).numpy()

print(y_data.shape)
for idx, decoded_img in enumerate(decoded_imgs):
    fig, ax = plt.subplots(1,3)
    ax[0].imshow(x_data[idx])
    ax[1].imshow(decoded_img)
    ax[2].imshow(y_data[idx])

print(encoded_imgs.shape)
print(encoded_imgs.shape[-1])
print(encoded_imgs[0,:,:,0].shape)
for idx in range(encoded_imgs.shape[-1]):
    plt.figure()
    plt.imshow(encoded_imgs[0,:,:,idx])

# Real image
img = cv2.imread('./b.jpg')
img = img[1080//2:1080//2+256,1920//2:1920//2+16,0]
img = img[tf.newaxis,...,tf.newaxis]
img = tf.image.per_image_standardization(x_data)
real_encoded_imgs = autoencoder.encoder(img).numpy()
real_decoded_imgs = autoencoder.decoder(real_encoded_imgs).numpy()
plt.figure()
plt.imshow(img[0])
plt.figure()
plt.imshow(real_decoded_imgs[0])
plt.show()
# %%
