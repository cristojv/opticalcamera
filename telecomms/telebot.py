import telegram
import argparse
import json

def send_message(message, secrets_filename):
    with open(secrets_filename) as json_file:
        secrets = json.load(json_file)

        token = str(secrets["token"])
        chat_id = int(secrets["chat_id"])

        request = telegram.utils.request.Request(read_timeout=2)
        bot = telegram.Bot(token, request=request)
        s = bot.send_message(chat_id, text=message, parse_mode=None,
                            disable_notification=False,
                            disable_web_page_preview=False)

if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument("-m","--message", required=True, type=str,
                    help="Message to send")
    ap.add_argument("-s","--secretsfilename", required=True, type=str,
                    help="Secrets filename containing a JSON with the format"\
                        "{token: <bot token>, chat_id: <chat id>}")
    args = vars(ap.parse_args())
    msg = str(args["message"])
    secrets_filename = str(args["secretsfilename"])
    send_message(msg, secrets_filename)